LOCAL_PATH := $(call my-dir)
include $(CLEAR_VARS)

LOCAL_MODULE := libvlcjni
LOCAL_LDFLAGS := -Wl,--build-id
LOCAL_SRC_FILES := \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/Android.mk \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/aout.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/Application.mk \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/egl-android-info.txt \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/eventfd.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/libvlcjni-equalizer.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/libvlcjni-medialist.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/libvlcjni-track.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/libvlcjni-util.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/libvlcjni.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/native_crash_handler.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/pipe2.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/pthread-condattr.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/pthread-once.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/pthread-rwlocks.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/sem.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/thumbnailer.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/vout.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcpcpy.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcpncpy.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcscasecmp.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcscat.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcschr.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcscmp.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcscoll.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcscpy.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcscspn.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsdup.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcslcat.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcslcpy.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcslen.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsncasecmp.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsncat.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsncmp.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsncpy.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsnlen.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcspbrk.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsrchr.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsspn.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsstr.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcstok.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcswidth.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wcsxfrm.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wmemchr.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wmemcmp.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wmemcpy.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wmemmove.c \
	/Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni/wchar/wmemset.c \

LOCAL_C_INCLUDES += /Users/nguyenvanhien/videostreamapp/vLC1/src/main/jni
LOCAL_C_INCLUDES += /Users/nguyenvanhien/videostreamapp/vLC1/src/release/jni

include $(BUILD_SHARED_LIBRARY)
