package com.video.film.stream.models;

import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.movies.Movie;

import java.util.ArrayList;

/**
 * Created by nguyenvanhien on 7/7/17.
 */

public class WatchList extends BaseModel {
    private ArrayList<Movie> movies;
    private ArrayList<TvChanel> tvChanels;

    public ArrayList<Movie> getMovies() {
        return movies;
    }

    public void setMovies(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public ArrayList<TvChanel> getTvChanels() {
        return tvChanels;
    }

    public void setTvChanels(ArrayList<TvChanel> tvChanels) {
        this.tvChanels = tvChanels;
    }
}
