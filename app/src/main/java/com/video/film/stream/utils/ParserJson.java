package com.video.film.stream.utils;

import android.util.Log;

import com.google.android.exoplayer2.text.Subtitle;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.video.film.stream.models.WatchList;
import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.sub.SubTitle;
import com.video.film.stream.models.tvshows.Episode;
import com.video.film.stream.models.tvshows.Seasons;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.models.tvshows.TvShowDetail;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by nguyenvanhien on 7/2/17.
 */

public class ParserJson {

    public static ArrayList<Seasons> getSeasonEpisodeList(String json) throws JSONException {
        ArrayList<Seasons> list = new ArrayList<>();
        JSONObject parent = new JSONObject(json);
        JSONObject result = parent.getJSONObject("result");
        JSONObject season = result.getJSONObject("seasons");
        Iterator x = season.keys();
        JSONArray jsonArraySeason = new JSONArray();

        while (x.hasNext()) {
            String key = (String) x.next();
            Log.e("KEY", "" + key + ": " + season.get(key).toString());
            ArrayList<Episode> episodes = new ArrayList<>();
            if (!key.equals("next")) {
                Seasons s = new Seasons();
                s.setSeasonName(key);
                JSONObject episode = season.getJSONObject(key);
                if (episode != null) {
                    Iterator y = episode.keys();
                    while (y.hasNext()) {
                        String keyEpisode = (String) y.next();
                        Episode m = new Episode();
                        JSONObject detail = episode.getJSONObject(keyEpisode);
                        m.setName(keyEpisode);
                        if (detail != null) {
                            if (detail.has("title")) {
                                if (detail.getString("title") != null) {
                                    m.setTitle(detail.getString("title"));
                                }
                            }
                            if (detail.has("plot")) {
                                if (detail.getString("plot") != null) {
                                    m.setPlot(detail.getString("plot"));
                                }
                            }
                            if (detail.has("released")) {
                                if (detail.getString("released") != null) {
                                    m.setReleased(detail.getString("released"));
                                }
                            }
                            if (detail.has("screenshot")) {
                                if (detail.getString("screenshot") != null) {
                                    m.setScreenshot(detail.getString("screenshot"));
                                }
                            }
                            episodes.add(m);
                        }
                    }
                    s.setEpisodes(episodes);
                }
                list.add(s);
            }
        }

        /*if(jsonArraySeason.length()>0){
            for(int i = 0 ; i < jsonArraySeason.length() ; i++){
                JSONObject episode = jsonArraySeason.getJSONObject(i);
                if(episode!=null){
                    Iterator y = episode.keys();
                    JSONArray jsonArrayEpisode = new JSONArray();
                    while (y.hasNext()){
                        String key = (String) y.next();
                        jsonArrayEpisode.put(season.get(key));
                    }
                }
            }
        }*/


        return list;
    }

    public static WatchList getWatchListMovie(String json) throws JSONException{
        ArrayList<Movie> movieArrayList = new ArrayList<>();
        ArrayList<TvChanel> tvChanelsList = new ArrayList<>();

        WatchList watchList = new WatchList();

        JSONObject parent = new JSONObject(json);
        JSONObject result = parent.getJSONObject("result");
        JSONObject imdb = result.getJSONObject("imdb");
        JSONObject channel = result.getJSONObject("channels");
        Iterator x = imdb.keys();

        while (x.hasNext()){
            String key = (String) x.next();
            if(StringUtils.isNotEmpty(key)){
                JSONObject movieObject = imdb.getJSONObject(key);
                if(movieObject!=null) {
                    Movie movie = getMovieFromJson(movieObject.toString());
                    if(movie!=null){
                        movieArrayList.add(movie);
                    }
                }

            }
        }

        Iterator y = channel.keys();

        while (y.hasNext()){
            String key = (String) y.next();
            if(StringUtils.isNotEmpty(key)){
                JSONObject tvChannelObject = channel.getJSONObject(key);
                if(tvChannelObject!=null) {
                    TvChanel chanel = getLiveTvFromJson(tvChannelObject.toString());
                    if(chanel!=null){
                        tvChanelsList.add(chanel);
                    }
                }

            }
        }

        watchList.setMovies(movieArrayList);
        watchList.setTvChanels(tvChanelsList);

        return watchList;
    }

    public static ArrayList<SubTitle> getSubTitleList(String json) throws JSONException {
        ArrayList<SubTitle> list = new ArrayList<>();
        JsonObject o = new JsonParser().parse(json).getAsJsonObject();

        for(Map.Entry<String, JsonElement> entry : o.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
            SubTitle subTitle = new SubTitle();
            subTitle.setCountryName(entry.getKey());
            subTitle.setSubUrl(String.valueOf(entry.getValue()));
            list.add(subTitle);
        }
        return list;
    }

    public static ArrayList<SubTitle> getSubTitleListTvShow(String json, String season, String episode) throws JSONException {
        ArrayList<SubTitle> list = new ArrayList<>();
        JsonObject o = new JsonParser().parse(json).getAsJsonObject();

        for(Map.Entry<String, JsonElement> entry : o.entrySet()) {
            System.out.println(entry.getKey());
            System.out.println(entry.getValue());
            /*SubTitle subTitle = new SubTitle();
            subTitle.setCountryName(entry.getKey());
            subTitle.setSubUrl(String.valueOf(entry.getValue()));*/
            //SubTitle subTitle = new SubTitle();
            if(entry.getKey().equals(season)){
                JsonObject episodeJson = new JsonParser().parse(entry.getValue().toString()).getAsJsonObject();
                for(Map.Entry<String, JsonElement> entryEpisode : episodeJson.entrySet()) {
                    if(entryEpisode.getKey().equals(episode)){
                        return getSubTitleList(entryEpisode.getValue().toString());
                    }
                }
                //list.add(subTitle);
                break;
            }


        }
        return list;
    }

    public static Movie getMovieFromJson(String json){
        Gson gson = new Gson();
        Movie movie = new Movie();
        movie = gson.fromJson(json,Movie.class);
        return movie;
    }

    public static TvChanel getLiveTvFromJson(String json){
        Gson gson = new Gson();
        TvChanel tvChanel = new TvChanel();
        tvChanel = gson.fromJson(json,TvChanel.class);
        return tvChanel;
    }

    public static SubTitle getSubTitleFromJson(String json){
        Gson gson = new Gson();
        SubTitle sub = new SubTitle();
        sub = gson.fromJson(json,SubTitle.class);
        return sub;
    }
}
