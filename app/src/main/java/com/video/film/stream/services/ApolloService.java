package com.video.film.stream.services;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class ApolloService {
    public static final String APOLLO_BASE_URL = "http://json.apollogroup.tv";
    public static final String APOLLO_API_BASE_URL = "http://api.apollogroup.tv/";

    public static ApolloApi createService(){
        if(apolloApi == null){
            apolloApi = getRetrofit().create(ApolloApi.class);
        }
        return apolloApi;
    }

    public static ApolloApi createServiceString(){
        if(apolloApiString == null){
            apolloApiString = getRetrofitAPIString().create(ApolloApi.class);
        }
        return apolloApiString;
    }

    public static ApolloApi createServiceApi(){
        if(apolloApiService == null){
            apolloApiService = getRetrofitAPI().create(ApolloApi.class);
        }
        return apolloApiService;
    }
    public static ApolloApi apolloApi;
    public static ApolloApi apolloApiService;
    public static ApolloApi apolloApiString;

    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(APOLLO_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    private static Retrofit getRetrofitAPI() {
        return new Retrofit.Builder()
                .baseUrl(APOLLO_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }

    private static Retrofit getRetrofitAPIString() {
        return new Retrofit.Builder()
                .baseUrl(APOLLO_API_BASE_URL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();
    }


}
