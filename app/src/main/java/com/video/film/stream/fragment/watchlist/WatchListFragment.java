package com.video.film.stream.fragment.watchlist;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.video.film.stream.ApolloApplication;
import com.video.film.stream.R;
import com.video.film.stream.activity.LoginDialog;
import com.video.film.stream.callbacks.SelectorListener;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.HeaderWatchListView;
import com.video.film.stream.dialog.ProcessDialog;
import com.video.film.stream.fragment.LiveTvParentFragment;
import com.video.film.stream.fragment.MovieParentFragment;
import com.video.film.stream.fragment.TvShowsParentFragment;
import com.video.film.stream.fragment.tvshows.TvShowFragment;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.movies.MovieCategory;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 7/5/17.
 */

public class WatchListFragment extends Fragment {
    @Bind(R.id.header)
    HeaderWatchListView header;

    public WatchListFragment() {
    }

    private Context mContext;
    private UserLoader userLoader;
    private MovieWatchListFragment movieWatchListFragment;
    private WatchListTvShowFragment watchListTvShowFragment;
    private WatchListLiveTvFragment watchListLiveTvFragment;

    private ArrayList<Movie> movies = new ArrayList<>();
    private ArrayList<TvShow> tvShows = new ArrayList<>();
    private ArrayList<TvChanel> tvChanels = new ArrayList<>();

    private LoginDialog loginDialog;
    private ProcessDialog processDialog;
    private boolean isLoadedData = false;
    private int currentIndex = 0;


    public static WatchListFragment newInstance() {
        WatchListFragment fragment = new WatchListFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_watchlist, container, false);
        ButterKnife.bind(this, view);
        mContext = view.getContext();
        processDialog = new ProcessDialog(mContext);
        loginDialog = new LoginDialog(mContext, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                loadData();
            }

            @Override
            public void failed() {

            }
        });

        userLoader = UserLoader.getInstance();
        header.setOnClickCallBack(new SelectorListener() {
            @Override
            public void onSelected(int pos) {
                showFragmentPosition(pos);
            }
        });

        loadData();


        return view;
    }

    private void initFragment(ArrayList<Movie> listMovie, ArrayList<TvChanel> listChannel) {
        if (listMovie != null && listMovie.size() >= 0) {
            movies.clear();
            tvShows.clear();
            tvChanels.clear();
            for (Movie item : listMovie) {
                if (item.getBanner() == null) {
                    movies.add(item);
                } else {
                    tvShows.add(item.getInfoTvShowFromIbdm());
                }
            }
            if (listChannel != null) {
                tvChanels = listChannel;
            }
            movieWatchListFragment = MovieWatchListFragment.newInstance(movies);
            watchListTvShowFragment = WatchListTvShowFragment.newInstance(tvShows);
            watchListLiveTvFragment = WatchListLiveTvFragment.newInstance(tvChanels);
            showFragmentPosition(0);
        }
    }

    private void loadData() {
        processDialog.show();
        Utils.checkValidToken(ApolloApplication.getInstance().getAppSettings().getApolloToken()
                , new SimpleCallBack() {
                    @Override
                    public void success(Object... params) {
                        userLoader.getWatchList(new SimpleCallBack() {
                            @Override
                            public void success(Object... params) {
                                processDialog.dismiss();
                                if (params[0] != null && params[1] != null) {
                                    initFragment((ArrayList<Movie>) params[0], (ArrayList<TvChanel>) params[1]);
                                    ApolloApplication.getInstance()
                                            .getAppSettings()
                                            .addWatchListWhenLoad((ArrayList<Movie>) params[0], (ArrayList<TvChanel>) params[1]);
                                    isLoadedData = true;
                                }
                            }

                            @Override
                            public void failed() {
                                processDialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void failed() {
                        processDialog.dismiss();
                        loginDialog.show();
                    }
                });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
    }

    private void showFragmentPosition(int pos) {
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        switch (pos) {
            case 0:
                transaction.replace(R.id.content, movieWatchListFragment, MovieWatchListFragment.class.getName());
                transaction.commitAllowingStateLoss();
                currentIndex = 0;
                break;
            case 1:
                transaction.replace(R.id.content, watchListTvShowFragment, WatchListTvShowFragment.class.getName());
                transaction.commitAllowingStateLoss();
                currentIndex = 1;
                break;
            case 2:
                transaction.replace(R.id.content, watchListLiveTvFragment, WatchListLiveTvFragment.class.getName());
                transaction.commit();
                currentIndex = 2;
                break;
        }

    }

}
