package com.video.film.stream.services.reponses.movies;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.video.film.stream.models.BaseModel;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.tvshows.TvShow;

import java.util.ArrayList;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class TvShowListResponse extends BaseModel {
    @SerializedName("result")
    @Expose
    private ArrayList<TvShow> tvShowList = new ArrayList<>();

    public ArrayList<TvShow> getTvShowList() {
        return tvShowList;
    }

}
