package com.video.film.stream.models.tvshows;

import android.os.Parcel;
import android.os.Parcelable;

import com.video.film.stream.models.BaseModel;
import com.video.film.stream.utils.Utils;

import java.util.ArrayList;

/**
 * Created by nguyenvanhien on 7/2/17.
 */

public class TvShow extends BaseModel implements Parcelable{
    private String poster;
    private String fanart;
    private String runtime;
    private String plot;
    private String director;
    private String writer;
    private String tvdb;
    private String tmdb;
    private String imdb;
    private String title;
    private String year;
    private String rating;
    private String votes;
    private String released;
    private String genres;
    private String link;

    private SeasonInfo seasons;
    private ArrayList<Seasons> seasonDetailList;
    public TvShow(){

    }

    protected TvShow(Parcel in) {
        poster = in.readString();
        fanart = in.readString();
        runtime = in.readString();
        plot = in.readString();
        director = in.readString();
        writer = in.readString();
        tvdb = in.readString();
        tmdb = in.readString();
        imdb = in.readString();
        title = in.readString();
        year = in.readString();
        rating = in.readString();
        votes = in.readString();
        released = in.readString();
        genres = in.readString();
        link = in.readString();
        seasons = in.readParcelable(SeasonInfo.class.getClassLoader());
        seasonDetailList = in.createTypedArrayList(Seasons.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(poster);
        dest.writeString(fanart);
        dest.writeString(runtime);
        dest.writeString(plot);
        dest.writeString(director);
        dest.writeString(writer);
        dest.writeString(tvdb);
        dest.writeString(tmdb);
        dest.writeString(imdb);
        dest.writeString(title);
        dest.writeString(year);
        dest.writeString(rating);
        dest.writeString(votes);
        dest.writeString(released);
        dest.writeString(genres);
        dest.writeString(link);
        dest.writeParcelable(seasons, flags);
        dest.writeTypedList(seasonDetailList);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TvShow> CREATOR = new Creator<TvShow>() {
        @Override
        public TvShow createFromParcel(Parcel in) {
            return new TvShow(in);
        }

        @Override
        public TvShow[] newArray(int size) {
            return new TvShow[size];
        }
    };

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getFanart() {
        return fanart;
    }

    public void setFanart(String fanart) {
        this.fanart = fanart;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getTvdb() {
        return tvdb;
    }

    public void setTvdb(String tvdb) {
        this.tvdb = tvdb;
    }

    public String getTmdb() {
        return tmdb;
    }

    public void setTmdb(String tmdb) {
        this.tmdb = tmdb;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public SeasonInfo getSeasons() {
        return seasons;
    }

    public String getTimeRelease(){
        return Utils.getDateFromSecond(this.getReleased());
    }

    public ArrayList<Seasons> getSeasonDetailList() {
        return seasonDetailList;
    }

    public void setSeasonDetailList(ArrayList<Seasons> seasonDetailList) {
        this.seasonDetailList = seasonDetailList;
    }
}
