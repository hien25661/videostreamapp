package com.video.film.stream.fragment.profile;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.video.film.stream.R;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.Profile;
import com.video.film.stream.utils.StringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 7/8/17.
 */

public class ProfileFragment extends Fragment {
    @Bind(R.id.tvName)
    CustomTextView tvName;
    @Bind(R.id.tvAccout)
    CustomTextView tvAccout;
    @Bind(R.id.tvExpire)
    CustomTextView tvExpire;
    Profile profile;

    public ProfileFragment(){}
    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_profile, container, false);
        ButterKnife.bind(this,view);
        loadData();
        return view;
    }

    private void loadData() {
        UserLoader.getInstance().loadProfile(new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                if(params[0]!=null && params[0] instanceof Profile){
                    profile = (Profile) params[0];
                    setData(profile);
                }
            }

            @Override
            public void failed() {

            }
        });
    }

    private void setData(Profile profile) {
        if(StringUtils.isNotEmpty(profile.getNicename())) {
            profile.setNicename("  "+profile.getNicename());
            tvName.setText(profile.getNicename());
        }
        if(StringUtils.isNotEmpty(profile.getAccount())) {
            tvAccout.setText(profile.getAccount());
        }
        String expireText = profile.getExpire();
        if(StringUtils.isNotEmpty(expireText)) {
            expireText = expireText.replaceAll("\"","");
            expireText = expireText.replaceAll("\\u005B","");
            expireText = expireText.replaceAll("\\u005D","");

            String[] listExpireTime = expireText.split(",");
            if(listExpireTime!=null && listExpireTime.length > 0) {
                tvExpire.setExpireText(listExpireTime[0]);
            }
        }
    }
}
