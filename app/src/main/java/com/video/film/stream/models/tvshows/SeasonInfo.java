package com.video.film.stream.models.tvshows;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nguyenvanhien on 7/2/17.
 */

public class SeasonInfo implements Parcelable{
    private LastSeason last;
    private NextSeason next;


    protected SeasonInfo(Parcel in) {
        last = in.readParcelable(LastSeason.class.getClassLoader());
        next = in.readParcelable(NextSeason.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(last, flags);
        dest.writeParcelable(next, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<SeasonInfo> CREATOR = new Creator<SeasonInfo>() {
        @Override
        public SeasonInfo createFromParcel(Parcel in) {
            return new SeasonInfo(in);
        }

        @Override
        public SeasonInfo[] newArray(int size) {
            return new SeasonInfo[size];
        }
    };

    public LastSeason getLast() {
        return last;
    }

    public NextSeason getNext() {
        return next;
    }
}
