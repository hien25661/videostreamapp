package com.video.film.stream.customview;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.video.film.stream.R;
import com.video.film.stream.callbacks.SelectorListener;
import com.video.film.stream.callbacks.SimpleCallBack;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyenvanhien on 6/28/17.
 */

public class CustomBottomBar extends RelativeLayout implements View.OnClickListener {
    SelectorListener callBack;
    @Bind(R.id.imvMovie)
    ImageView imvMovie;
    @Bind(R.id.imvTvShow)
    ImageView imvTvShow;
    @Bind(R.id.imvTvChanel)
    ImageView imvTvChanel;
    @Bind(R.id.imvWatchList)
    ImageView imvWatchList;
    @Bind(R.id.imvProfile)
    ImageView imvProfile;
    @Bind(R.id.imvLogout)
    ImageView imvLogout;

    @Bind(R.id.tvMovie)
    CustomTextView tvMovie;
    @Bind(R.id.tvTvShows)
    CustomTextView tvTvShows;
    @Bind(R.id.tvLiveTv)
    CustomTextView tvLiveTv;
    @Bind(R.id.tvWatchList)
    CustomTextView tvWatchList;
    @Bind(R.id.tvProfile)
    CustomTextView tvProfile;
    @Bind(R.id.tvLogout)
    CustomTextView tvLogout;

    @Bind(R.id.viewMovie)
    LinearLayout viewMovie;
    @Bind(R.id.viewTvShows)
    LinearLayout viewTvShows;
    @Bind(R.id.viewTvChanel)
    LinearLayout viewTvChanel;
    @Bind(R.id.viewWatchList)
    LinearLayout viewWatchList;
    @Bind(R.id.viewProfile)
    LinearLayout viewProfile;
    @Bind(R.id.viewLogout)
    LinearLayout viewLogout;



    int currentPos = -1;

    public CustomBottomBar(Context context) {
        super(context);
        init();
    }

    public CustomBottomBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CustomBottomBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View v = inflate(getContext(), R.layout.layout_bottom_bar, this);
        ButterKnife.bind(this, v);
        viewMovie.setOnClickListener(this);
        viewTvShows.setOnClickListener(this);
        viewTvChanel.setOnClickListener(this);
        viewWatchList.setOnClickListener(this);
        viewProfile.setOnClickListener(this);
        viewLogout.setOnClickListener(this);
        setSelected(0);
    }

    public void setOnClickCallBack(SelectorListener callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewMovie:
                setSelected(0);
                if(callBack!=null){
                    callBack.onSelected(0);
                }
                break;
            case R.id.viewTvShows:
                setSelected(1);
                if(callBack!=null){
                    callBack.onSelected(1);
                }
                break;
            case R.id.viewTvChanel:
                setSelected(2);
                if(callBack!=null){
                    callBack.onSelected(2);
                }
                break;

            case R.id.viewWatchList:
                setSelected(3);
                if(callBack!=null){
                    callBack.onSelected(3);
                }
                break;
            case R.id.viewProfile:
                setSelected(4);
                if(callBack!=null){
                    callBack.onSelected(4);
                }
                break;
            case R.id.viewLogout:
                setSelected(5);
                if(callBack!=null){
                    callBack.onSelected(5);
                }
                break;
            default:
                break;
        }
    }

    public void setSelected(int pos) {
        if (currentPos != pos) {
            currentPos = pos;
            imvMovie.setImageResource(R.mipmap.ic_movie_tab_unselected);
            imvTvShow.setImageResource(R.mipmap.ic_tvshow_tab_unselec);
            imvTvChanel.setImageResource(R.mipmap.ic_tvshow);
            imvWatchList.setImageResource(R.mipmap.ic_watchlist_tab_unselect);
            imvProfile.setImageResource(R.mipmap.ic_prof_tab_unselect);
            imvLogout.setImageResource(R.mipmap.ic_prof_tab_unselect);

            tvMovie.setTextColor(Color.WHITE);
            tvTvShows.setTextColor(Color.WHITE);
            tvLiveTv.setTextColor(Color.WHITE);
            tvWatchList.setTextColor(Color.WHITE);
            tvProfile.setTextColor(Color.WHITE);
            tvLogout.setTextColor(Color.WHITE);


            switch (pos) {
                case 0:
                    imvMovie.setImageResource(R.mipmap.ic_movie_tab_select);
                    tvMovie.setTextColor(Color.parseColor("#E1331A"));
                    break;
                case 1:
                    imvTvShow.setImageResource(R.mipmap.ic_tvshow_tab_select);
                    tvTvShows.setTextColor(Color.parseColor("#2BBAE0"));
                    break;
                case 2:
                    imvTvChanel.setImageResource(R.mipmap.ic_tvshow_select);
                    tvLiveTv.setTextColor(Color.parseColor("#e9cd26"));
                    break;
                case 3:
                    imvWatchList.setImageResource(R.mipmap.ic_watchlist_tab_unselect);
                    tvWatchList.setTextColor(Color.parseColor("#2BBAE0"));
                    break;
                case 4:
                    imvProfile.setImageResource(R.mipmap.ic_prof_tab_select);
                    tvProfile.setTextColor(Color.parseColor("#337ab7"));
                    break;
                case 5:
                    imvLogout.setImageResource(R.mipmap.ic_prof_tab_select);
                    tvLogout.setTextColor(Color.parseColor("#e9cd26"));
                    break;
            }
        }
    }
}

