package com.video.film.stream;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.devbrackets.android.exomedia.ExoMedia;
import com.google.android.exoplayer2.ext.okhttp.OkHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.upstream.TransferListener;
import com.squareup.leakcanary.LeakCanary;
import com.video.film.stream.player.manager.PlaylistManager;
import com.video.film.stream.utils.AppSettings;

import okhttp3.OkHttpClient;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by nguyenvanhien on 6/28/17.
 */

public class ApolloApplication extends MultiDexApplication {
    static ApolloApplication instance;
    private Typeface customFont;
    private AppSettings appSettings;
    private static PlaylistManager playlistManager;

    public void onCreate() {
        super.onCreate();
        instance = this;
        appSettings = new AppSettings(this);
        customFont = Typeface.createFromAsset(getAssets(), "fonts/Lato-Regular.ttf");
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/Lato-Regular.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        playlistManager = new PlaylistManager();
        //LeakCanary.install(this);

        configureExoMedia();
    }

    public AppSettings getAppSettings() {
        return appSettings;
    }

    public static ApolloApplication getInstance() {
        return instance;
    }

    public Typeface getCustomFont() {
        return customFont;
    }

    @Override
    protected void attachBaseContext(Context base) {
        MultiDex.install(ApolloApplication.this);
        super.attachBaseContext(base);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        instance = null;
        playlistManager = null;
    }
    public static PlaylistManager getPlaylistManager() {
        return playlistManager;
    }
    private void configureExoMedia() {
        // Registers the media sources to use the OkHttp client instead of the standard Apache one
        // Note: the OkHttpDataSourceFactory can be found in the ExoPlayer extension library `extension-okhttp`
        ExoMedia.setHttpDataSourceFactoryProvider(new ExoMedia.HttpDataSourceFactoryProvider() {
            @NonNull
            @Override
            public HttpDataSource.BaseFactory provide(@NonNull String userAgent, @Nullable TransferListener<? super DataSource> listener) {
                return new OkHttpDataSourceFactory(new OkHttpClient(), userAgent, listener);
            }
        });
    }
}
