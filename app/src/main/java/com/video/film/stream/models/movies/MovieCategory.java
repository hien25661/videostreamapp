package com.video.film.stream.models.movies;

import android.os.Parcel;
import android.os.Parcelable;

import com.video.film.stream.models.BaseModel;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class MovieCategory extends BaseModel implements Parcelable {
    private String id;
    private String name;

    protected MovieCategory(Parcel in) {
        id = in.readString();
        name = in.readString();
    }

    public static final Creator<MovieCategory> CREATOR = new Creator<MovieCategory>() {
        @Override
        public MovieCategory createFromParcel(Parcel in) {
            return new MovieCategory(in);
        }

        @Override
        public MovieCategory[] newArray(int size) {
            return new MovieCategory[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
    }
}
