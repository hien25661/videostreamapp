package com.video.film.stream.adapters.TvChanel;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.video.film.stream.R;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.dialog.DialogInfoLiveTv;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.player.FullScreenVideoPlayerActivity;
import com.video.film.stream.player.VideoPlayerActivity;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.WatchListAction;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class TvChanelListAdapter extends UltimateViewAdapter<TvChanelListAdapter.ViewHolder> {
    private ArrayList<TvChanel> tvShows;
    private Context mContext;
    private boolean isWatchList = false;

    public TvChanelListAdapter(ArrayList<TvChanel> tvshows) {
        this.tvShows = tvshows;
    }

    public TvChanelListAdapter(ArrayList<TvChanel> tvshows,boolean isWatchList) {
        this.tvShows = tvshows;
        this.isWatchList = isWatchList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tvshows, parent, false);
        ViewHolder vh = new ViewHolder(v);
        mContext = v.getContext();
        return vh;
    }

    @Override
    public ViewHolder newFooterHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder newHeaderHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position >= 0 && position < tvShows.size()) {
            final TvChanel tvShow = tvShows.get(position);
            if (tvShow != null) {
                Utils.showImageFromUrlIntoView(mContext, tvShow.getLogo(), holder.imvFilm, position);
                holder.tvTitle.setText(tvShow.getName());
               /* if(tvShow.getSeasons()!=null){
                    LastSeason lastSeason = tvShow.getSeasons().getLast();
                    NextSeason nextSeason = tvShow.getSeasons().getNext();

                    if(lastSeason!=null){
                        *//*holder.tvSeasonInfo.setText(tvShow.getYear() + " - " + lastSeason.getSeason()
                                + " SEASONS" + " - " + Utils.formatRating(tvShow.getRating()));*//*
                        holder.tvSeasonInfo.setInfoSeason(tvShow);
                        holder.tvLast.setText("Last: " + Utils.getDateFromSecondSeason(lastSeason.getReleased()));
                    }
                    if(nextSeason!=null){
                        holder.tvNext.setNextSeasonText(tvShow);
                    }
                }*/
                holder.tvSeasonInfo.setText("");
                String liveNow = "", liveNext = "";
                if(tvShow.getEpg_now()!=null){
                    liveNow = tvShow.getEpg_now();
                }
                if(tvShow.getEpg_next()!=null){
                    liveNext = tvShow.getEpg_next();
                }
                holder.tvLast.setText("Live Now: " + liveNow);
                holder.tvNext.setVisibility(View.VISIBLE);
                holder.tvNext.setText("Live Next: " + liveNext);

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogInfoLiveTv dialogInfo = new DialogInfoLiveTv(mContext);
                        dialogInfo.setTvChanel(tvShow);
                        dialogInfo.setDeleteCallback(new SelectorListenerObject() {
                            @Override
                            public void onSelected(Object... params) {
                                if(isWatchList){
                                    if(params[0]!=null){
                                        if(params[0] == WatchListAction.ADD){
                                            tvShows.add(tvShow);
                                            notifyDataSetChanged();
                                        }else {
                                            if(params[0] == WatchListAction.DELETE){
                                                tvShows.remove(tvShow);
                                                notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        });
                        dialogInfo.show();
                        /*Utils.checkValidToken(ApolloApplication.
                                        getInstance().getAppSettings().getApolloToken(),
                                new SimpleCallBack() {
                                    @Override
                                    public void success(Object... params) {
                                        playTvChannel(tvShow);
                                    }

                                    @Override
                                    public void failed() {

                                    }
                                });*/

                    }
                });
                holder.tvLast.setVisibility(View.VISIBLE);
                holder.tvNext.setVisibility(View.VISIBLE);
                if(isWatchList){
                    holder.tvLast.setVisibility(View.GONE);
                    holder.tvNext.setVisibility(View.GONE);
                }
            }
        }
    }

    private void playTvChannel(final TvChanel tvShow) {
        UserLoader userLoader = UserLoader.getInstance();
        userLoader.playTvChannel(tvShow
                , new SimpleCallBack() {
                    @Override
                    public void success(Object... params) {
                        if (params[0] != null && params[0] instanceof String) {
                            tvShow.setLink((String) params[0]);
                            Intent intent = new Intent(mContext, FullScreenVideoPlayerActivity.class);
                            intent.putExtra(VideoPlayerActivity.TVCHANNEL, tvShow);
                            mContext.startActivity(intent);
                        }
                    }

                    @Override
                    public void failed() {
                    }
                });


    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getAdapterItemCount() {
        return tvShows.size();
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imvFilm)
        ImageView imvFilm;
        @Bind(R.id.tvTitle)
        CustomTextView tvTitle;
        @Bind(R.id.tvSeasonInfo)
        CustomTextView tvSeasonInfo;
        @Bind(R.id.tvLast)
        CustomTextView tvLast;
        @Bind(R.id.tvNext)
        CustomTextView tvNext;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        try {
            if (holder != null) {
                Glide.clear(holder.imvFilm);
            }
        } catch (Exception ex) {
        }
        super.onViewRecycled(holder);
    }
}
