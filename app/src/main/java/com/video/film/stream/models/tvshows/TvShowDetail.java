package com.video.film.stream.models.tvshows;

import android.os.Parcel;
import android.os.Parcelable;

import com.video.film.stream.models.BaseModel;

/**
 * Created by nguyenvanhien on 7/2/17.
 */

public class TvShowDetail extends BaseModel implements Parcelable {
    private String poster;
    private String fanart;
    private String runtime;
    private String plot;
    private String director;
    private String writer;
    private String tvdb;
    private String tmdb;
    private String imdb;
    private String title;
    private String year;
    private String rating;
    private String votes;
    private String released;
    private String genres;

    private Seasons seasons;


    protected TvShowDetail(Parcel in) {
        poster = in.readString();
        fanart = in.readString();
        runtime = in.readString();
        plot = in.readString();
        director = in.readString();
        writer = in.readString();
        tvdb = in.readString();
        tmdb = in.readString();
        imdb = in.readString();
        title = in.readString();
        year = in.readString();
        rating = in.readString();
        votes = in.readString();
        released = in.readString();
        genres = in.readString();
        seasons = in.readParcelable(Seasons.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(poster);
        dest.writeString(fanart);
        dest.writeString(runtime);
        dest.writeString(plot);
        dest.writeString(director);
        dest.writeString(writer);
        dest.writeString(tvdb);
        dest.writeString(tmdb);
        dest.writeString(imdb);
        dest.writeString(title);
        dest.writeString(year);
        dest.writeString(rating);
        dest.writeString(votes);
        dest.writeString(released);
        dest.writeString(genres);
        dest.writeParcelable(seasons, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TvShowDetail> CREATOR = new Creator<TvShowDetail>() {
        @Override
        public TvShowDetail createFromParcel(Parcel in) {
            return new TvShowDetail(in);
        }

        @Override
        public TvShowDetail[] newArray(int size) {
            return new TvShowDetail[size];
        }
    };

    public String getPoster() {
        return poster;
    }

    public String getFanart() {
        return fanart;
    }

    public String getRuntime() {
        return runtime;
    }

    public String getPlot() {
        return plot;
    }

    public String getDirector() {
        return director;
    }

    public String getWriter() {
        return writer;
    }

    public String getTvdb() {
        return tvdb;
    }

    public String getTmdb() {
        return tmdb;
    }

    public String getImdb() {
        return imdb;
    }

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getRating() {
        return rating;
    }

    public String getVotes() {
        return votes;
    }

    public String getReleased() {
        return released;
    }

    public String getGenres() {
        return genres;
    }

    public Seasons getSeasons() {
        return seasons;
    }

    public void setSeasons(Seasons seasons) {
        this.seasons = seasons;
    }
}
