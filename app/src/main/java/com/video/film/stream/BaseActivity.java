package com.video.film.stream;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.bumptech.glide.Glide;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class BaseActivity extends AppCompatActivity {
    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        recycleActivityView();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        recycleActivityView();
        super.onDestroy();
    }

    public void recycleActivityView(){
        try{
            Glide.get(this).getBitmapPool().clearMemory();
            Glide.get(this).clearDiskCache();
            Glide.get(this).clearMemory();
        }catch (Exception ex){}
    }
}
