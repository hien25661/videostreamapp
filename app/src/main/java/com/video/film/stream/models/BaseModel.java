package com.video.film.stream.models;

import com.google.gson.Gson;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class BaseModel {
    public String toJson(){
        if(this!=null){
            return new Gson().toJson(this);
        }
        return null;
    }
}
