package com.video.film.stream.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.bumptech.glide.Glide;
import com.video.film.stream.BaseActivity;
import com.video.film.stream.R;
import com.video.film.stream.adapters.tvshows.SeasonDialogAdapter;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.customview.tvshow.SeasonView;
import com.video.film.stream.dialog.ProcessDialog;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.tvshows.Episode;
import com.video.film.stream.models.tvshows.Seasons;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyenvanhien on 7/1/17.
 */

public class TvShowDetailActivity extends BaseActivity {
    @Bind(R.id.imvPoster)
    ImageView imvPoster;
    @Bind(R.id.tvTitle)
    CustomTextView tvTitle;
    @Bind(R.id.tvInfo)
    CustomTextView tvInfo;
    @Bind(R.id.tvNumLikes)
    CustomTextView tvNumLikes;
    @Bind(R.id.tvNumComments)
    CustomTextView tvNumComments;
    @Bind(R.id.tvRating)
    CustomTextView tvRating;
    @Bind(R.id.tvSynopis)
    CustomTextView tvSynopis;
    @Bind(R.id.tvYear)
    CustomTextView tvYear;
    @Bind(R.id.seasonview)
    SeasonView seasonView;
    @Bind(R.id.tvNameSeason)
    CustomTextView tvNameSeason;

    @Bind(R.id.imvChooseSeason)
    ImageView imvChooseSeason;
    @Bind(R.id.scvParent)
    NestedScrollView scvParent;
    @Bind(R.id.rcvAllSeason)
    RecyclerView rcvAllSeason;

    private TvShow tvShow;
    private UserLoader userLoader;
    private ProcessDialog processDialog;
    private LoginDialog loginDialog;
    private DialogChooseSeason dialogChooseSeason;

    private int currentIndexSeason = 0;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_tvshow);
        ButterKnife.bind(this);
        userLoader = UserLoader.getInstance();
        processDialog = new ProcessDialog(this);
        loginDialog = new LoginDialog(this, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                playMovie();
            }

            @Override
            public void failed() {

            }
        });
        initData();
    }

    private void initData() {
        tvShow = getIntent().getParcelableExtra(Consts.MOVIE);
        if (tvShow != null) {
            loadInfoDetail(tvShow);
            initPopup();
            loadAllSeasonView();
        }

    }

    private void initPopup() {
        if (tvShow.getSeasonDetailList() != null) {
            dialogChooseSeason = new DialogChooseSeason(this, tvShow.getSeasonDetailList());
        }
    }

    private void loadAllSeasonView() {
        if (tvShow.getSeasonDetailList() != null && tvShow.getSeasonDetailList().size() > 0) {
            if(Utils.isGoogleTV(this)){
                rcvAllSeason.setLayoutManager(new GridLayoutManager(this,3));
            } else {
                rcvAllSeason.setLayoutManager(new LinearLayoutManager(this));
            }
            rcvAllSeason.setHasFixedSize(false);
            currentIndexSeason = tvShow.getSeasonDetailList().size() - 1;
            SeasonDialogAdapter allSeasonAdapter = new SeasonDialogAdapter(
                    tvShow.getSeasonDetailList(),
                    1,
                    new SelectorListenerObject() {
                        @Override
                        public void onSelected(Object... params) {
                            if (params[0] != null && params[0] instanceof Seasons) {
                                Seasons item = (Seasons) params[0];
                                if (item != null) {
                                    setSeasonName(item);
                                }
                            }
                            if (params[1] != null && params[1] instanceof Integer) {
                                currentIndexSeason = (int) params[1];
                            }
                        }
                    }
            ,currentIndexSeason);
            rcvAllSeason.setAdapter(allSeasonAdapter);
        }
    }

    private void loadInfoDetail(TvShow movie) {
        Utils.showImageFromUrlIntoView(this, movie.getFanart(), imvPoster, 0);
        tvTitle.setText(movie.getTitle());
        tvInfo.setText(movie.getGenres() + " - " + movie.getTimeRelease());
        tvNumComments.setCommentText(movie.getRuntime());
        tvNumLikes.setLikeText(movie.getVotes());
        tvRating.setRatingBigTvShow(movie.getRating());
        tvSynopis.setText(movie.getPlot());
        tvYear.setYearTextTvShow("TV-SHOW", movie.getYear());

        loadEpisode();
        scvParent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // Ready, move up
                scvParent.scrollTo(0, 0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    scvParent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    scvParent.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        scvParent.requestChildFocus(imvPoster, imvPoster);
    }

    private void loadEpisode() {
        seasonView.setCallBackSelectedEpisode(new SelectorListenerObject() {
            @Override
            public void onSelected(Object... params) {
                if (params[0] != null && params[0] instanceof Episode) {
                    showDetailEpisode((Episode) params[0]);
                }
            }
        });
        if (tvShow.getSeasonDetailList() != null && tvShow.getSeasonDetailList().size() > 0) {
            ArrayList<Seasons> listSeason = tvShow.getSeasonDetailList();
            setSeasonName(listSeason.get(listSeason.size() - 1));
        }
    }

    private void setSeasonName(Seasons season) {
        if (season != null) {
            seasonView.processData(season);
            tvNameSeason.setText("EPISODE: SEASON " + season.getSeasonName());
        }
    }

    private void showDetailEpisode(Episode episode) {
        Intent intent = new Intent(this, EpisodeDetailActivity.class);
        intent.putExtra(Consts.MOVIE, tvShow);
        intent.putExtra(Consts.EPISODE, episode);
        intent.putExtra("season_pos", currentIndexSeason);
        startActivity(intent);
    }

    @OnClick(R.id.imvBack)
    public void onBack() {
        onBackPressed();
    }

    private void playMovie() {
    }

    @OnClick(R.id.imvChooseSeason)
    public void chooseSeason() {
        if (dialogChooseSeason != null && !dialogChooseSeason.isShowing()) {
            dialogChooseSeason.show();
        }
    }

    private class DialogChooseSeason extends Dialog {
        RecyclerView rcvPopUp;
        private ArrayList<Seasons> seasons;
        private SeasonDialogAdapter seasonDialogAdapter;
        private Context context;

        public DialogChooseSeason(@NonNull Context context, ArrayList<Seasons> seasons) {
            super(context);
            this.seasons = seasons;
            this.context = context;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.popup_choose_season);
            ButterKnife.bind(this);
            getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                    WindowManager.LayoutParams.WRAP_CONTENT);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            rcvPopUp = (RecyclerView) findViewById(R.id.rcvPopUp);
            seasonDialogAdapter = new SeasonDialogAdapter(seasons, 0, new SelectorListenerObject() {
                @Override
                public void onSelected(Object... params) {
                    if (params[0] != null && params[0] instanceof Seasons) {
                        Seasons item = (Seasons) params[0];
                        if (item != null) {
                            setSeasonName(item);
                            dismiss();
                        }
                    }
                    if (params[1] != null && params[1] instanceof Integer) {
                        currentIndexSeason = (int) params[1];
                    }
                }
            },currentIndexSeason);
            rcvPopUp.setHasFixedSize(false);
            rcvPopUp.setLayoutManager(new LinearLayoutManager(context));
            rcvPopUp.setAdapter(seasonDialogAdapter);

        }
    }

    @Override
    protected void onDestroy() {
        try {
            Glide.clear(imvPoster);
        } catch (Exception e) {
        }
        super.onDestroy();
    }
}
