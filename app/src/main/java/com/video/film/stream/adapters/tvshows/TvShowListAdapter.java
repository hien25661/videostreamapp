package com.video.film.stream.adapters.tvshows;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.video.film.stream.R;
import com.video.film.stream.adapters.movies.MovieListAdapter;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.dialog.DialogInfoTvShow;
import com.video.film.stream.models.tvshows.LastSeason;
import com.video.film.stream.models.tvshows.NextSeason;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.WatchListAction;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class TvShowListAdapter extends UltimateViewAdapter<TvShowListAdapter.ViewHolder>{
    private ArrayList<TvShow> tvShows;
    private Context mContext;
    private boolean isWatchList = false;

    public TvShowListAdapter(ArrayList<TvShow> tvshows) {
        this.tvShows = tvshows;
    }

    public TvShowListAdapter(ArrayList<TvShow> tvshows, boolean isWatchList) {
        this.tvShows = tvshows;
        this.isWatchList = isWatchList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tvshows, parent, false);
        ViewHolder vh = new ViewHolder(v);
        mContext = v.getContext();
        return vh;
    }

    @Override
    public ViewHolder newFooterHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder newHeaderHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (position >= 0 && position < tvShows.size()) {
            final TvShow tvShow = tvShows.get(position);
            if (tvShow != null) {
                if(Utils.isGoogleTV(mContext)) {
                    Utils.showImageFromUrlIntoView(mContext, tvShow.getPoster(), holder.imvFilm, position);
                }else {
                    Utils.showImageFromUrlIntoView(mContext, tvShow.getFanart(), holder.imvFilm, position);
                }
                holder.tvTitle.setText(tvShow.getTitle());
                if(!isWatchList) {
                    if (tvShow.getSeasons() != null) {
                        LastSeason lastSeason = tvShow.getSeasons().getLast();
                        NextSeason nextSeason = tvShow.getSeasons().getNext();

                        if (lastSeason != null) {
                        /*holder.tvSeasonInfo.setText(tvShow.getYear() + " - " + lastSeason.getSeason()
                                + " SEASONS" + " - " + Utils.formatRating(tvShow.getRating()));*/
                            holder.tvSeasonInfo.setInfoSeason(tvShow);
                            holder.tvLast.setText("Last: " + Utils.getDateFromSecondSeason(lastSeason.getReleased()));
                        }
                        if (nextSeason != null) {
                            holder.tvNext.setNextSeasonText(tvShow);
                        }
                    }
                }else {
                    holder.tvSeasonInfo.setInfoSeasonWatchList(tvShow);
                    holder.tvLast.setVisibility(View.GONE);
                }

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogInfoTvShow dialogInfo = new DialogInfoTvShow(mContext);
                        dialogInfo.setTvShow(tvShow);
                        dialogInfo.setFromWatchList(isWatchList);
                        dialogInfo.show();
                        dialogInfo.setDeleteCallback(new SelectorListenerObject() {
                            @Override
                            public void onSelected(Object... params) {
                                if(isWatchList){
                                    if(params[0]!=null){
                                        if(params[0] == WatchListAction.ADD){
                                            tvShows.add(tvShow);
                                            notifyDataSetChanged();
                                        }else {
                                            if(params[0] == WatchListAction.DELETE){
                                                Log.e("AAAA","DELETE");
                                                tvShows.remove(tvShow);
                                                notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        });
                    }
                });
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getAdapterItemCount() {
        return tvShows.size();
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imvFilm)
        ImageView imvFilm;
        @Bind(R.id.tvTitle)
        CustomTextView tvTitle;
        @Bind(R.id.tvSeasonInfo)
        CustomTextView tvSeasonInfo;
        @Bind(R.id.tvLast)
        CustomTextView tvLast;
        @Bind(R.id.tvNext)
        CustomTextView tvNext;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        try {
            if (holder != null) {
                Glide.clear(holder.imvFilm);
            }
        } catch (Exception ex) {
        }
        super.onViewRecycled(holder);
    }
}
