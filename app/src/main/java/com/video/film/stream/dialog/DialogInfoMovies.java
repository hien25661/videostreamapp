package com.video.film.stream.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.video.film.stream.ApolloApplication;
import com.video.film.stream.R;
import com.video.film.stream.activity.FullscreenActivity;
import com.video.film.stream.activity.LoginDialog;
import com.video.film.stream.activity.MediaPlayerSubtitle;
import com.video.film.stream.activity.MovieDetailActivity;
import com.video.film.stream.activity.VideoViewSubtitleNew;
import com.video.film.stream.callbacks.CallBack;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.loader.MovieLoader;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.sub.SubTitle;
import com.video.film.stream.player.FullScreenVideoPlayerActivity;
import com.video.film.stream.player.VideoPlayerActivity;
import com.video.film.stream.utils.AppSettings;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.StringUtils;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.WatchListAction;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyenvanhien on 6/28/17.
 */

public class DialogInfoMovies extends Dialog {
    @Bind(R.id.imvPoster)
    ImageView imvPoster;
    @Bind(R.id.tvName)
    TextView tvName;
    @Bind(R.id.tvInfo)
    TextView tvInfo;
    @Bind(R.id.tvRating)
    CustomTextView tvRating;

    @Bind(R.id.imvWatchList)
    ImageView imvWatchList;

    private Movie movie;
    private Context mContext;
    private UserLoader userLoader;
    private LoginDialog loginDialog;
    private ProcessDialog processDialog;
    private AppSettings appSettings;
    private boolean isActionAdd = false;

    private SelectorListenerObject deleteCallback;
    private DialogChooseSub dialogChooseSub;

    public DialogInfoMovies(@NonNull Context context) {
        super(context, R.style.full_screen_dialog);
        this.mContext = context;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_info);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);
        appSettings = ApolloApplication.getInstance().getAppSettings();
        userLoader = UserLoader.getInstance();
        processDialog = new ProcessDialog(mContext);
        loginDialog = new LoginDialog(mContext, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                if (isActionAdd) {
                    addOrRemoveWatchList();
                    isActionAdd = false;
                } else {
                    playMovie();
                }
            }

            @Override
            public void failed() {

            }
        });
        setData();
    }

    private void setData() {
        if (movie != null) {
            Utils.showImageFromUrlIntoView(mContext, movie.getPoster(), imvPoster, 0);
            tvName.setText(movie.getTitle() + "(" + movie.getYear() + ")");
            tvInfo.setText(movie.getGenres().replaceAll(" ", "").replaceAll("\\W", ",")
                    + "\n" + Utils.getDateFromSecond(movie.getReleased()));
            tvRating.setRatingSmall(movie.getRating());
            if (appSettings.isImdbBelongWatchList(movie.getImdb())) {
                imvWatchList.setImageResource(R.mipmap.ic_remove_watchlist);
            } else {
                imvWatchList.setImageResource(R.mipmap.ic_top_dialog);
            }
        }
    }

    @OnClick(R.id.btnClose)
    public void closeDialog() {
        this.dismiss();
    }

    @OnClick(R.id.btnWatch)
    public void watch() {
        /*if(!Utils.checkRelogin()){
            loginDialog.show();
        }else {
            playMovie();
        }*/
        Utils.checkValidToken(ApolloApplication.getInstance().getAppSettings().getApolloToken(),
                new SimpleCallBack() {
                    @Override
                    public void success(Object... params) {
                        playMovie();
                    }

                    @Override
                    public void failed() {
                        loginDialog.show();
                    }
                });

    }

    private void playMovie() {
        processDialog.show();
        MovieLoader movieLoader = MovieLoader.getInstance();
        movieLoader.searchSubTitle(movie.getImdb(), new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                ArrayList<SubTitle> subList = new ArrayList<SubTitle>();
                subList = (ArrayList<SubTitle>) params[0];
                processDialog.dismiss();
                if(subList !=null && subList.size() >0) {
                    SubTitle header = new SubTitle();
                    header.setHeader(true);
                    header.setCountryName("None");
                    subList.add(0,header);
                    dialogChooseSub = new DialogChooseSub(
                            mContext, subList, new SelectorListenerObject() {
                        @Override
                        public void onSelected(Object... params) {
                            if (params[0] != null && params[0] instanceof SubTitle) {
                                startMoviePlayer((SubTitle) params[0]);
                            }
                        }
                    }
                    );
                    dialogChooseSub.show();
                }else {
                    startMoviePlayer(null);
                }

            }

            @Override
            public void failed() {
                startMoviePlayer(null);
            }
        });

    }

    private void startMoviePlayer(final SubTitle sub) {
        userLoader.playMovie(movie, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                processDialog.dismiss();
                if (params[0] != null && params[0] instanceof String) {
                    movie.setLink((String) params[0]);
                    Intent intent = new Intent(mContext, VideoViewSubtitleNew.class);
                    intent.putExtra("url", movie.getLink());
                    intent.putExtra("name", movie.getTitle());
                    if (sub != null) {
                        if (StringUtils.isNotEmpty(sub.getSubUrl())) {
                            intent.putExtra("sub", sub.getSubUrl());
                        }
                    }
                    mContext.startActivity(intent);
                    if(dialogChooseSub!=null && dialogChooseSub.isShowing()){
                        dialogChooseSub.dismiss();
                    }
                }
            }

            @Override
            public void failed() {
                processDialog.dismiss();
            }
        });
    }


    @OnClick(R.id.btnShowDetail)
    public void showDetail() {
        Intent intent = new Intent(this.mContext, MovieDetailActivity.class);
        intent.putExtra(Consts.MOVIE, movie);
        this.mContext.startActivity(intent);
    }

    @OnClick(R.id.imvWatchList)
    public void addToWatchList() {
        isActionAdd = true;
        Utils.checkValidToken(ApolloApplication.getInstance().getAppSettings().getApolloToken()
                , new SimpleCallBack() {
                    @Override
                    public void success(Object... params) {
                        addOrRemoveWatchList();
                    }

                    @Override
                    public void failed() {
                        loginDialog.show();
                    }
                });

    }

    public void addOrRemoveWatchList() {
        if (!appSettings.isImdbBelongWatchList(movie.getImdb())) {
            userLoader.addWatchListImdb(movie.getImdb(), new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    imvWatchList.setImageResource(R.mipmap.ic_remove_watchlist);
                    if (deleteCallback != null) {
                        deleteCallback.onSelected(WatchListAction.ADD);
                    }
                }

                @Override
                public void failed() {

                }
            });

        } else {
            userLoader.removeWatchListImdb(movie.getImdb(), new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    imvWatchList.setImageResource(R.mipmap.ic_top_dialog);
                    if (deleteCallback != null) {
                        deleteCallback.onSelected(WatchListAction.DELETE);
                    }
                }

                @Override
                public void failed() {

                }
            });

        }
    }

    public void setDeleteCallback(SelectorListenerObject deleteCallback) {
        this.deleteCallback = deleteCallback;
    }
}
