package com.video.film.stream.callbacks;

import java.util.Objects;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public interface SimpleCallBack {
    void success(Object... params);
    void failed();
}
