package com.video.film.stream.customview;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.Nullable;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import com.video.film.stream.ApolloApplication;
import com.video.film.stream.R;
import com.video.film.stream.models.tvshows.LastSeason;
import com.video.film.stream.models.tvshows.NextSeason;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.utils.StringUtils;
import com.video.film.stream.utils.Utils;

import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by nguyenvanhien on 6/28/17.
 */

public class CustomTextView extends TextView {


    public CustomTextView(Context context) {
        super(context);
        init(context);
    }

    public CustomTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        Typeface tf = ApolloApplication.getInstance().getCustomFont();
        this.setTypeface(tf);
    }

    public void setRatingSmall(String rating) {
        rating = Utils.formatRating(rating);
        this.setBackgroundResource(R.drawable.circle_shape);
        String text = rating + "/10";
        Spannable span = new SpannableString(text);
        span.setSpan(new RelativeSizeSpan(1.5f), 0, rating.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new RelativeSizeSpan(0.6f), rating.length(), text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new ForegroundColorSpan(Color.parseColor("#E1331A"))
                , 0, rating.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setText(span);
    }
    public void setRatingSmallTvShow(String rating) {
        rating = Utils.formatRating(rating);
        this.setBackgroundResource(R.drawable.circle_shape_tvshow);
        String text = rating + "/10";
        Spannable span = new SpannableString(text);
        span.setSpan(new RelativeSizeSpan(1.5f), 0, rating.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new RelativeSizeSpan(0.6f), rating.length(), text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new ForegroundColorSpan(Color.parseColor("#2BBAE0"))
                , 0, rating.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setText(span);
    }
    public void setRatingBig(String rating) {
        rating = Utils.formatRating(rating);
        this.setBackgroundResource(R.drawable.circle_shape);
        String text = rating + "/10";
        Spannable span = new SpannableString(text);
        span.setSpan(new RelativeSizeSpan(2.8f), 0, rating.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new RelativeSizeSpan(1f), rating.length(), text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new ForegroundColorSpan(Color.parseColor("#E1331A"))
                , 0, rating.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setText(span);
    }

    public void setRatingBigTvShow(String rating) {
        rating = Utils.formatRating(rating);
        this.setBackgroundResource(R.drawable.circle_shape_tvshow);
        String text = rating + "/10";
        Spannable span = new SpannableString(text);
        span.setSpan(new RelativeSizeSpan(2.8f), 0, rating.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new RelativeSizeSpan(1f), rating.length(), text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        span.setSpan(new ForegroundColorSpan(Color.parseColor("#2BBAE0"))
                , 0, rating.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        setText(span);
    }

    public void setLikeText(String value) {
        if (StringUtils.isEmpty(value)) value = "0";
        String numbLike = NumberFormat.getInstance(Locale.US)
                .format(Integer.parseInt(value));
        this.setText(numbLike + " likes");
    }

    public void setCommentText(String value) {
        if (StringUtils.isEmpty(value)) value = "0";
        this.setText("+" + value + " comments");
    }

    public void setYearText(String type, String year) {
        String text = type + " " + year;
        Spannable span = new SpannableString(text);
        span.setSpan(new ForegroundColorSpan(Color.parseColor("#E1331A"))
                , 0, type.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        this.setText(span);
    }

    public void setYearTextTvShow(String type, String year) {
        String text = type + " " + year;
        Spannable span = new SpannableString(text);
        span.setSpan(new ForegroundColorSpan(Color.parseColor("#2BBAE0"))
                , 0, type.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        this.setText(span);
    }

    public void setInfoSeason(TvShow tvShow) {
        try {
            String year = tvShow.getYear();
            int nbeason = Integer.parseInt(tvShow.getSeasons().getLast().getSeason());
            String numbSeason = nbeason+(nbeason > 1 ? " seasons" : " season");
            String rating = Utils.formatRating(tvShow.getRating());

            String text = year + " - " + numbSeason + " " + rating;

            Spannable span = new SpannableString(text);
            span.setSpan(new ForegroundColorSpan(Color.parseColor("#2BBAE0"))
                    , text.length() - rating.length(), text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            this.setText(span);
        } catch (Exception ex) {

        }
    }

    public void setInfoSeasonWatchList(TvShow tvShow) {
        try {
            String year = tvShow.getYear();
            String rating = Utils.formatRating(tvShow.getRating());

            String text = year + " - - " + rating;

            Spannable span = new SpannableString(text);
            span.setSpan(new ForegroundColorSpan(Color.parseColor("#2BBAE0"))
                    , text.length() - rating.length(), text.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            this.setText(span);
        } catch (Exception ex) {

        }
    }

    public void setNextSeasonText(TvShow tvShow){

        NextSeason nextSeason = tvShow.getSeasons().getNext();
        try{
            long nextRelease = Long.parseLong(nextSeason.getReleased());
            long lastRelease = System.currentTimeMillis()/1000;

            long day = nextRelease - lastRelease;

            long numbDay = (long) day/(24*3600);
            this.setText("Next: "+numbDay + (numbDay > 1 ? " days" : " day") + " (" + Utils.getDateFromSecondSeason(""+nextRelease) + ")");
        }catch (Exception ex){

        }
    }

    public void setExpireText(String expireText) {
            String expire = "Expire ";


            String text = expire +expireText;

            Spannable span = new SpannableString(text);
            span.setSpan(new ForegroundColorSpan(Color.WHITE)
                    , 0, expire.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            this.setText(span);
    }


}
