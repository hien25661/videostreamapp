package com.video.film.stream.fragment.watchlist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.video.film.stream.R;
import com.video.film.stream.adapters.movies.MovieListAdapter;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.loader.MovieLoader;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.movies.MovieCategory;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.ViewType;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class MovieWatchListFragment extends Fragment {
    @Bind(R.id.rcvMovie)
    UltimateRecyclerView rcvMovie;

    private Context mContext;
    private MovieCategory movieCategory;
    private MovieListAdapter movieListAdapter;
    private MovieLoader movieLoader;
    private ArrayList<Movie> movies;
    private boolean isFirstTime = true;

    public MovieWatchListFragment(){

    }
    public static MovieWatchListFragment newInstance(ArrayList<Movie> movies) {
        MovieWatchListFragment fragment = new MovieWatchListFragment();
        Bundle bundle = new Bundle(1);
        bundle.putParcelableArrayList(Consts.WATCHLIST_MOVIE, movies);
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        movieLoader = MovieLoader.getInstance();
        if(movieListAdapter == null && isFirstTime) {
            loadData();
            isFirstTime = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        ButterKnife.bind(this,view);
        mContext = view.getContext();
        movieLoader = MovieLoader.getInstance();
        if(movieListAdapter == null) {
            loadData();
        }else {
            initView();
            rcvMovie.setAdapter(movieListAdapter);
        }
        return view;
    }

    private void initView() {
        rcvMovie.setHasFixedSize(true);
        int valueSpant = 2;
        if(Utils.isGoogleTV(mContext)){
            valueSpant = 4;
        }
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(valueSpant,
                StaggeredGridLayoutManager.VERTICAL);
        rcvMovie.setLayoutManager(layoutManager);
    }

    private void loadData() {
        movies = getArguments().getParcelableArrayList(Consts.WATCHLIST_MOVIE);
        if(movies!=null && movies.size()>0){
            setAdapterMovie(movies);
        }
    }

    private void setAdapterMovie(ArrayList<Movie> movies) {
        initView();
        movieListAdapter = new MovieListAdapter(movies,true);
        rcvMovie.setAdapter(movieListAdapter);
    }

}
