package com.video.film.stream.models.movies;

import android.os.Parcel;
import android.os.Parcelable;

import com.video.film.stream.models.BaseModel;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.utils.Utils;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class Movie extends BaseModel implements Parcelable{
    private String poster;
    private String fanart;
    private String banner;
    private String runtime;
    private String plot;
    private String director;
    private String writer;
    private String tvdb;
    private String tmdb;
    private String imdb;
    private String title;
    private String year;
    private String rating;
    private String votes;
    private String released;
    private String genres;
    private String link;

    public Movie(){}
    protected Movie(Parcel in) {
        poster = in.readString();
        fanart = in.readString();
        runtime = in.readString();
        plot = in.readString();
        director = in.readString();
        writer = in.readString();
        tvdb = in.readString();
        tmdb = in.readString();
        imdb = in.readString();
        title = in.readString();
        year = in.readString();
        rating = in.readString();
        votes = in.readString();
        released = in.readString();
        genres = in.readString();
        link = in.readString();
        banner = in.readString();
    }

    public static final Creator<Movie> CREATOR = new Creator<Movie>() {
        @Override
        public Movie createFromParcel(Parcel in) {
            return new Movie(in);
        }

        @Override
        public Movie[] newArray(int size) {
            return new Movie[size];
        }
    };

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getFanart() {
        return fanart;
    }

    public void setFanart(String fanart) {
        this.fanart = fanart;
    }

    public String getRuntime() {
        return runtime;
    }

    public void setRuntime(String runtime) {
        this.runtime = runtime;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getTvdb() {
        return tvdb;
    }

    public void setTvdb(String tvdb) {
        this.tvdb = tvdb;
    }

    public String getTmdb() {
        return tmdb;
    }

    public void setTmdb(String tmdb) {
        this.tmdb = tmdb;
    }

    public String getImdb() {
        return imdb;
    }

    public void setImdb(String imdb) {
        this.imdb = imdb;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getVotes() {
        return votes;
    }

    public void setVotes(String votes) {
        this.votes = votes;
    }

    public String getReleased() {
        return released;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public String getGenres() {
        return genres;
    }

    public void setGenres(String genres) {
        this.genres = genres;
    }

    public String getTimeRelease(){
        return Utils.getDateFromSecond(this.getReleased());
    }


    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(poster);
        dest.writeString(fanart);
        dest.writeString(runtime);
        dest.writeString(plot);
        dest.writeString(director);
        dest.writeString(writer);
        dest.writeString(tvdb);
        dest.writeString(tmdb);
        dest.writeString(imdb);
        dest.writeString(title);
        dest.writeString(year);
        dest.writeString(rating);
        dest.writeString(votes);
        dest.writeString(released);
        dest.writeString(genres);
        dest.writeString(link);
        dest.writeString(banner);
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public TvShow getInfoTvShowFromIbdm(){
        TvShow tvShow = new TvShow();
        tvShow.setPoster(poster);

        tvShow.setFanart(fanart);
        tvShow.setRuntime(runtime);
        tvShow.setPlot(plot);
        tvShow.setDirector(director);
        tvShow.setWriter(writer);
        tvShow.setTvdb(tvdb);
        tvShow.setTmdb(tmdb);
        tvShow.setImdb(imdb);
        tvShow.setTitle(title);
        tvShow.setYear(year);
        tvShow.setRating(rating);
        tvShow.setVotes(votes);
        tvShow.setReleased(released);
        tvShow.setGenres(genres);
        tvShow.setLink(link);
        return tvShow;
    }
}
