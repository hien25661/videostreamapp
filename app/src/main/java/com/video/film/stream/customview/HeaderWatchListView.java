package com.video.film.stream.customview;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.video.film.stream.R;
import com.video.film.stream.callbacks.SelectorListener;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/28/17.
 */

public class HeaderWatchListView extends RelativeLayout implements View.OnClickListener {
    SelectorListener callBack;
    @Bind(R.id.imvMovie)
    ImageView imvMovie;
    @Bind(R.id.imvTvShow)
    ImageView imvTvShow;
    @Bind(R.id.imvTvChanel)
    ImageView imvTvChanel;

    @Bind(R.id.tvMovie)
    CustomTextView tvMovie;
    @Bind(R.id.tvTvShows)
    CustomTextView tvTvShows;
    @Bind(R.id.tvLiveTv)
    CustomTextView tvLiveTv;

    @Bind(R.id.viewMovie)
    LinearLayout viewMovie;
    @Bind(R.id.viewTvShows)
    LinearLayout viewTvShows;
    @Bind(R.id.viewTvChanel)
    LinearLayout viewTvChanel;

    @Bind(R.id.viewSelect1)
    LinearLayout viewSelect1;
    @Bind(R.id.viewSelect2)
    LinearLayout viewSelect2;
    @Bind(R.id.viewSelect3)
    LinearLayout viewSelect3;

    int currentPos = -1;

    public HeaderWatchListView(Context context) {
        super(context);
        init();
    }

    public HeaderWatchListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public HeaderWatchListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        View v = inflate(getContext(), R.layout.header_watchlist, this);
        ButterKnife.bind(this, v);
        viewMovie.setOnClickListener(this);
        viewTvShows.setOnClickListener(this);
        viewTvChanel.setOnClickListener(this);
        setSelected(0);
    }

    public void setOnClickCallBack(SelectorListener callBack) {
        this.callBack = callBack;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewMovie:
                setSelected(0);
                if(callBack!=null){
                    callBack.onSelected(0);
                }
                break;
            case R.id.viewTvShows:
                setSelected(1);
                if(callBack!=null){
                    callBack.onSelected(1);
                }
                break;
            case R.id.viewTvChanel:
                setSelected(2);
                if(callBack!=null){
                    callBack.onSelected(2);
                }
                break;
            default:
                break;
        }
    }

    private void setSelected(int pos) {
        if (currentPos != pos) {
            currentPos = pos;
            imvMovie.setImageResource(R.mipmap.ic_movie_tab_unselected);
            imvTvShow.setImageResource(R.mipmap.ic_tvshow_tab_unselec);
            imvTvChanel.setImageResource(R.mipmap.ic_tvshow);

            tvMovie.setTextColor(Color.WHITE);
            tvTvShows.setTextColor(Color.WHITE);
            tvLiveTv.setTextColor(Color.WHITE);

            viewSelect1.setVisibility(INVISIBLE);
            viewSelect2.setVisibility(INVISIBLE);
            viewSelect3.setVisibility(INVISIBLE);


            switch (pos) {
                case 0:
                    imvMovie.setImageResource(R.mipmap.ic_movie_tab_select);
                    tvMovie.setTextColor(Color.parseColor("#E1331A"));
                    viewSelect1.setBackgroundColor(Color.parseColor("#E1331A"));
                    viewSelect1.setVisibility(VISIBLE);
                    break;
                case 1:
                    imvTvShow.setImageResource(R.mipmap.ic_tvshow_tab_select);
                    tvTvShows.setTextColor(Color.parseColor("#2BBAE0"));
                    viewSelect2.setBackgroundColor(Color.parseColor("#2BBAE0"));
                    viewSelect2.setVisibility(VISIBLE);
                    break;
                case 2:
                    imvTvChanel.setImageResource(R.mipmap.ic_tvshow_select);
                    tvLiveTv.setTextColor(Color.parseColor("#e9cd26"));
                    viewSelect3.setBackgroundColor(Color.parseColor("#e9cd26"));
                    viewSelect3.setVisibility(VISIBLE);
                    break;
            }
        }
    }
}

