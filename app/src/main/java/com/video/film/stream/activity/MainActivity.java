package com.video.film.stream.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.video.film.stream.ApolloApplication;
import com.video.film.stream.BaseActivity;
import com.video.film.stream.R;
import com.video.film.stream.callbacks.SelectorListener;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.CustomBottomBar;
import com.video.film.stream.fragment.LiveTvParentFragment;
import com.video.film.stream.fragment.MovieParentFragment;
import com.video.film.stream.fragment.TvShowsParentFragment;
import com.video.film.stream.fragment.liveTv.LiveTvSearchFragment;
import com.video.film.stream.fragment.movies.MovieFragment;
import com.video.film.stream.fragment.movies.MovieSearchFragment;
import com.video.film.stream.fragment.profile.ProfileFragment;
import com.video.film.stream.fragment.tvshows.TvShowSearchFragment;
import com.video.film.stream.fragment.watchlist.WatchListFragment;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.utils.StringUtils;
import com.video.film.stream.utils.Utils;

import org.w3c.dom.Text;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivity implements SearchView.OnQueryTextListener {
    @Bind(R.id.content)
    FrameLayout frContent;
    @Bind(R.id.tvTitle)
    TextView tvTitle;
    @Bind(R.id.bottomBar)
    CustomBottomBar bottomBar;
    @Bind(R.id.toolbar)
    Toolbar toolbar;

    private boolean isFirstTime = true;
    private MovieParentFragment movieParentFragment;
    private TvShowsParentFragment tvShowParentFragment;
    private LiveTvParentFragment liveTvParentFragment;
    private WatchListFragment watchListFragment;
    private ProfileFragment profileFragment;
    private LoginDialog loginDialog, loginDialogLogout;
    @Bind(R.id.search_view)
    SearchView searchView;
    private int currentIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                tvTitle.setVisibility(View.VISIBLE);
                showFragmentPosition(currentIndex);
                return false;
            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvTitle.setVisibility(View.GONE);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if(StringUtils.isNotEmpty(s)){
                    switch (currentIndex){
                        case 0:
                            showSearchFragmentMovie(s);
                            break;
                        case 1:
                            showSearchFragmentTvShow(s);
                            break;
                        case 2:
                            showLiveTvFragmentTvShow(s);
                            break;
                        default:break;
                    }

                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });
        loginDialog = new LoginDialog(this, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                addListWatch();
                initView();
                initBottomBar();
            }

            @Override
            public void failed() {

            }
        });
        loginDialogLogout = new LoginDialog(this, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                showFragmentPosition(currentIndex);
                bottomBar.setSelected(currentIndex);
            }

            @Override
            public void failed() {

            }
        });
        checkLogin();
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // thêm search vào vào action bar
        getMenuInflater().inflate(R.menu.menu_search, menu);
        MenuItem itemSearch = menu.findItem(R.id.search_view);
        searchView = (SearchView) itemSearch.getActionView();
        //set OnQueryTextListener cho search view để thực hiện search theo text
        searchView.setOnQueryTextListener(this);
        return true;
    }*/

    private void addListWatch() {
        UserLoader.getInstance().getWatchList(new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                if (params[0] != null && params[1] != null) {
                    ApolloApplication.getInstance()
                            .getAppSettings()
                            .addWatchListWhenLoad((ArrayList<Movie>) params[0]
                                    , (ArrayList<TvChanel>) params[1]);
                }
            }

            @Override
            public void failed() {

            }
        });
    }

    private void checkLogin() {

        Utils.checkValidToken(ApolloApplication.getInstance().getAppSettings().getApolloToken(),
                new SimpleCallBack() {
                    @Override
                    public void success(Object... params) {
                        addListWatch();
                        initView();
                        initBottomBar();
                    }

                    @Override
                    public void failed() {
                        loginDialog.show();
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void initView() {
        movieParentFragment = MovieParentFragment.newInstance();
        tvShowParentFragment = TvShowsParentFragment.newInstance();
        liveTvParentFragment = LiveTvParentFragment.newInstance();
        watchListFragment = WatchListFragment.newInstance();
        profileFragment = ProfileFragment.newInstance();
        showFragmentPosition(0);

    }

    private void showFragmentPosition(int pos) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        switch (pos) {
            case 0:
                currentIndex = 0;
                toolbar.setVisibility(View.VISIBLE);
                transaction.replace(R.id.content, movieParentFragment, MovieParentFragment.class.getName());
                transaction.commit();
                tvTitle.setTextColor(Color.parseColor("#E1331A"));
                tvTitle.setText(getString(R.string.movie));
                tvTitle.setVisibility(View.VISIBLE);
                searchView.onActionViewCollapsed();
                searchView.setVisibility(View.VISIBLE);
                break;
            case 1:
                currentIndex = 1;
                toolbar.setVisibility(View.VISIBLE);
                transaction.replace(R.id.content, tvShowParentFragment, TvShowsParentFragment.class.getName());
                transaction.commit();
                tvTitle.setTextColor(Color.parseColor("#2BBAE0"));
                tvTitle.setText(getString(R.string.tvshow));
                tvTitle.setVisibility(View.VISIBLE);
                searchView.onActionViewCollapsed();
                searchView.setVisibility(View.VISIBLE);
                break;
            case 2:
                currentIndex = 2;
                toolbar.setVisibility(View.VISIBLE);
                transaction.replace(R.id.content, liveTvParentFragment, LiveTvParentFragment.class.getName());
                transaction.commit();
                tvTitle.setTextColor(Color.parseColor("#e9cd26"));
                tvTitle.setText(getString(R.string.livetv));
                tvTitle.setVisibility(View.VISIBLE);
                searchView.onActionViewCollapsed();
                searchView.setVisibility(View.VISIBLE);
                break;
            case 3:
                currentIndex = 3;
                toolbar.setVisibility(View.VISIBLE);
                transaction.replace(R.id.content, watchListFragment, WatchListFragment.class.getName());
                transaction.commit();
                tvTitle.setTextColor(Color.WHITE);
                tvTitle.setText(getString(R.string.watch_list));
                searchView.setVisibility(View.GONE);
                break;
            case 4:
                currentIndex = 4;
                toolbar.setVisibility(View.GONE);
                transaction.replace(R.id.content, profileFragment, ProfileFragment.class.getName());
                transaction.commit();
                tvTitle.setTextColor(Color.WHITE);
                tvTitle.setText(getString(R.string.profile));
                searchView.setVisibility(View.GONE);
                break;
            case 5:
                ApolloApplication.getInstance().getAppSettings().setApolloToken("");
                loginDialogLogout.show();
                break;
        }

    }

    private void initBottomBar() {
        bottomBar.setOnClickCallBack(new SelectorListener() {
            @Override
            public void onSelected(int pos) {
                showFragmentPosition(pos);
            }
        });
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    private void showSearchFragmentMovie(String search){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, MovieSearchFragment.newInstance(search)
                , MovieSearchFragment.class.getName());
        transaction.commit();
    }

    private void showSearchFragmentTvShow(String search){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, TvShowSearchFragment.newInstance(search)
                , TvShowSearchFragment.class.getName());
        transaction.commit();
    }

    private void showLiveTvFragmentTvShow(String search){
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content, LiveTvSearchFragment.newInstance(search)
                , TvShowSearchFragment.class.getName());
        transaction.commit();
    }
}
