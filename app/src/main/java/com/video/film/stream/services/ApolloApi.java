package com.video.film.stream.services;

import com.video.film.stream.models.Profile;
import com.video.film.stream.services.reponses.LoginResponse;
import com.video.film.stream.services.reponses.movies.CheckTokenResponse;
import com.video.film.stream.services.reponses.movies.LiveTvListResponse;
import com.video.film.stream.services.reponses.movies.MovieCategoryResponse;
import com.video.film.stream.services.reponses.movies.MovieLinkResponse;
import com.video.film.stream.services.reponses.movies.MovieListResponse;
import com.video.film.stream.services.reponses.movies.TvShowListResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public interface ApolloApi {
    //Login, Get Token
    //http://api.apollogroup.tv/login/?email=sergio.devmail@gmail.com&password=12341234
    @GET("login/")
    Call<LoginResponse> getToken(@Query("email") String email, @Query("password") String pass);

    @GET
    Call<String> getToken(@Url String url);


    @GET("list/{type}/menu.json")
    Call<MovieCategoryResponse> getCategoriesMovieList(@Path("type") String type);

    @GET("list/{type}/{category_id}.json")
    Call<MovieListResponse> getMovieList(@Path("type") String type,
            @Path("category_id") String category_id);

    @GET("list/{type}/{category_id}.json")
    Call<TvShowListResponse> getTvShowList(@Path("type") String type,
                                           @Path("category_id") String category_id);

    @GET("list/{type}/{category_id}.json")
    Call<LiveTvListResponse> getLiveTvList(@Path("type") String type,
                                           @Path("category_id") String category_id);

    @GET
    Call<String> getTvShowDetail(@Url String url);

    @GET("play/")
    Call<MovieLinkResponse> playMovie(@Query("imdb") String imdbId, @Query("player") String player, @Query("token") String token);

    @GET("play/")
    Call<MovieLinkResponse> playTvShow(@Query("imdb") String imdbId,
                                       @Query("season") String season,
                                       @Query("episode") String episode,
                                       @Query("player") String player,
                                       @Query("token") String token);

    @GET("play/")
    Call<MovieLinkResponse> playTvChannel(@Query("channel") String channel,
                                          @Query("player") String player,
                                       @Query("token") String token);

    @GET("token/")
    Call<CheckTokenResponse> checkToken(@Query("token") String token);

    @GET
    Call<String> getWatchList(@Url String url, @Query("token") String token);

    @GET
    Call<String> addWatchListImdb(@Url String url, @Query("token") String token);

    @GET
    Call<String> addWatchListChannel(@Url String url, @Query("token") String token);

    @GET
    Call<String> removeWatchList(@Url String url, @Query("token") String token);

    @GET
    Call<MovieListResponse> searchMovie(@Url String url);

    @GET
    Call<TvShowListResponse> searchTvShow(@Url String url);

    @GET
    Call<LiveTvListResponse> searchLiveTv(@Url String url);


    @GET
    Call<Profile> getInfoProfile(@Url String url);

    @GET
    Call<String> searchSubTitle(@Url String url);
}
