package com.video.film.stream.fragment.watchlist;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.video.film.stream.R;
import com.video.film.stream.adapters.tvshows.TvShowListAdapter;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.loader.MovieLoader;
import com.video.film.stream.models.movies.MovieCategory;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.ViewType;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class WatchListTvShowFragment extends Fragment {
    @Bind(R.id.rcvMovie)
    UltimateRecyclerView rcvMovie;

    private Context mContext;
    private MovieCategory movieCategory;
    private TvShowListAdapter movieListAdapter;
    private MovieLoader movieLoader;
    private ArrayList<TvShow> tvShows;

    public WatchListTvShowFragment(){

    }

    public static WatchListTvShowFragment newInstance(ArrayList<TvShow> tvShows) {
        WatchListTvShowFragment fragment = new WatchListTvShowFragment();
        Bundle bundle = new Bundle(1);
        bundle.putParcelableArrayList(Consts.WATCHLIST_TVSHOWS, tvShows);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        ButterKnife.bind(this,view);
        mContext = view.getContext();
        movieLoader = MovieLoader.getInstance();
        initView();
        if(movieListAdapter == null) {
            loadData();
        }else {
            rcvMovie.setAdapter(movieListAdapter);
        }
        return view;
    }

    private void initView() {
        rcvMovie.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        if (Utils.isGoogleTV(mContext)) {
            rcvMovie.setLayoutManager(gridLayoutManager);
        } else {
            rcvMovie.setLayoutManager(layoutManager);
        }
    }

    private void loadData() {
        tvShows = getArguments().getParcelableArrayList(Consts.WATCHLIST_TVSHOWS);
        if(tvShows!=null && tvShows.size() > 0){
            setAdapterMovie(tvShows);
        }
    }

    private void setAdapterMovie(ArrayList<TvShow> movies) {
        movieListAdapter = new TvShowListAdapter(movies,true);
        rcvMovie.setAdapter(movieListAdapter);
    }
}
