/*
 * Copyright (C) 2013 yixia.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.video.film.stream.activity;

import android.app.Activity;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.video.film.stream.R;
import com.video.film.stream.callbacks.DownloadTask;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.dialog.ProcessDialog;
import com.video.film.stream.utils.StringUtils;

import java.io.File;

import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnTimedTextListener;
import io.vov.vitamio.Vitamio;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;

public class VideoViewSubtitleNew extends Activity implements MediaPlayer.OnInfoListener, MediaPlayer.OnBufferingUpdateListener {

	private String path = "";
	private String subtitle_path = "";
	private VideoView mVideoView;
	private TextView mSubtitleView;
	private long mPosition = 0;
	private int mVideoLayout = 0;
	private ProcessDialog processDialog;
	String url = "http://192.99.66.133:8081/vod/Game.Of.Thrones.S06.720p.HDTV.x264-BTN/Game.of.Thrones.S06E10.720p.HDTV.x264-AVS.mkv";
	File subFile;
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		if (!LibsChecker.checkVitamioLibs(this))
			return;
		Vitamio.isInitialized(getApplicationContext());
		setContentView(R.layout.subtitle2);
		mVideoView = (VideoView) findViewById(R.id.surface_view);
		mSubtitleView = (TextView) findViewById(R.id.subtitle_view);
		File extStore = Environment.getExternalStorageDirectory();
		subFile = new File(this.getFilesDir(),"a1.srt");
		processDialog = new ProcessDialog(this);
		processDialog.show();

		subtitle_path = getIntent().getStringExtra("sub");
		if(StringUtils.isNotEmpty(subtitle_path)){
			final DownloadTask task = new DownloadTask(this);
			task.execute(subtitle_path.replaceAll("\"",""));
			task.setCallBackCompleted(new SimpleCallBack() {
				@Override
				public void success(Object... params) {
					path = getIntent().getStringExtra("url");
					if (StringUtils.isEmpty(path)) {
						Toast.makeText(VideoViewSubtitleNew.this, "Cannot Play Video. Please Try again!", Toast.LENGTH_LONG).show();
						return;
					} else {
						settingVideo();
					}
				}

				@Override
				public void failed() {
					path = getIntent().getStringExtra("url");
					if (StringUtils.isEmpty(path)) {
						Toast.makeText(VideoViewSubtitleNew.this, "Cannot Play Video. Please Try again!", Toast.LENGTH_LONG).show();
						return;
					} else {
						settingVideo();
					}
				}
			});
		}else {
			path = getIntent().getStringExtra("url");
			if (StringUtils.isEmpty(path)) {
                processDialog.dismiss();
				Toast.makeText(VideoViewSubtitleNew.this, "Cannot Play Video. Please Try again!", Toast.LENGTH_LONG).show();
				return;
			} else {
				settingVideo();
			}
		}
	}

	private void settingVideo(){
		/*
			 * Alternatively,for streaming media you can use
			 * mVideoView.setVideoURI(Uri.parse(URLstring));
			 */
		if(getIntent().getBooleanExtra("liveTV",false)){
			//path = "http://158.69.229.90:8081/livetv/C010001/playlist.m3u8?wmsAuthSign=c2VydmVyX3RpbWU9Ny8zMC8yMDE3IDE6MzI6MjEgQU0maGFzaF92YWx1ZT0zdDNlYWp3a2U2clBDcDBmNWc4M2pnPT0mdmFsaWRtaW51dGVzPTEwJmlkPXNlbGVjdG9yPUs0UnZKSVgxOFV6LSNzZXJ2ZXI9MTU4LjY5LjIyOS45MCNpcD0xMTYuMTAwLjUzLjUwI3R5cGU9bGl2ZXR2I21lZGlhPWxpdmV0di9DMDEwMDAxI3RpbWU9MTUwMTM3ODM0MSN1c2VyaWQ9MTgwMSZzdHJtX2xlbj0xNA==";
			mVideoView.setVideoURI(Uri.parse(path));
		}else {
			mVideoView.setVideoPath(path);
			mVideoView.setMediaController(new MediaController(this));
		}

//		mVideoView.requestFocus();
//
//		mVideoView.requestFocus();
		mVideoView.setOnInfoListener(this);
		mVideoView.setOnBufferingUpdateListener(this);

		mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mediaPlayer) {
				processDialog.dismiss();
				// optional need Vitamio 4.0
				mediaPlayer.setPlaybackSpeed(1.0f);
				mediaPlayer.setVideoQuality(MediaPlayer.VIDEOQUALITY_LOW);
				mVideoView.requestFocus();
				mVideoView.addTimedTextSource(subFile.getAbsolutePath());
				if(StringUtils.isNotEmpty(subtitle_path)) {
					mVideoView.setTimedTextShown(true);
				}else {
					mVideoView.setTimedTextShown(false);
				}

			}
		});
		mVideoView.setOnTimedTextListener(new OnTimedTextListener() {

			@Override
			public void onTimedText(String text) {
				mSubtitleView.setText(text);
			}

			@Override
			public void onTimedTextUpdate(byte[] pixels, int width, int height) {

			}
		});
	}

	@Override
	protected void onPause() {
		mPosition = mVideoView.getCurrentPosition();
		mVideoView.stopPlayback();
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (mPosition > 0) {
			mVideoView.seekTo(mPosition);
			mPosition = 0;
		}
		super.onResume();
		mVideoView.start();
	}

	public void changeLayout(View view) {
		mVideoLayout++;
		if (mVideoLayout == 4) {
			mVideoLayout = 0;
		}
		switch (mVideoLayout) {
			case 0:
				mVideoLayout = VideoView.VIDEO_LAYOUT_ORIGIN;
				//view.setBackgroundResource(R.drawable.mediacontroller_sreen_size_100);
				break;
			case 1:
				mVideoLayout = VideoView.VIDEO_LAYOUT_SCALE;
				//view.setBackgroundResource(R.drawable.mediacontroller_screen_fit);
				break;
			case 2:
				mVideoLayout = VideoView.VIDEO_LAYOUT_STRETCH;
				//view.setBackgroundResource(R.drawable.mediacontroller_screen_size);
				break;
			case 3:
				mVideoLayout = VideoView.VIDEO_LAYOUT_ZOOM;
				//view.setBackgroundResource(R.drawable.mediacontroller_sreen_size_crop);

				break;
		}
		mVideoView.setVideoLayout(mVideoLayout, 0);
	}

	public void changeLayout(int mVideoLayout) {
		switch (mVideoLayout) {
			case 0:
				mVideoLayout = VideoView.VIDEO_LAYOUT_ORIGIN;
				//view.setBackgroundResource(R.drawable.mediacontroller_sreen_size_100);
				break;
			case 1:
				mVideoLayout = VideoView.VIDEO_LAYOUT_SCALE;
				//view.setBackgroundResource(R.drawable.mediacontroller_screen_fit);
				break;
			case 2:
				mVideoLayout = VideoView.VIDEO_LAYOUT_STRETCH;
				//view.setBackgroundResource(R.drawable.mediacontroller_screen_size);
				break;
			case 3:
				mVideoLayout = VideoView.VIDEO_LAYOUT_ZOOM;
				//view.setBackgroundResource(R.drawable.mediacontroller_sreen_size_crop);

				break;
		}
		mVideoView.setVideoLayout(mVideoLayout, 0);
	}
	private int findTrackIndexFor(int mediaTrackType, MediaPlayer.TrackInfo[] trackInfo) {
		int index = -1;
		for (int i = 0; i < trackInfo.length; i++) {
			if (trackInfo[i].getTrackType() == mediaTrackType) {
				return i;
			}
		}
		return index;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
			changeLayout(VideoView.VIDEO_LAYOUT_SCALE);
		}else {
			if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
				changeLayout(VideoView.VIDEO_LAYOUT_SCALE);
			}
		}
	}

	@Override
	public void onBufferingUpdate(MediaPlayer mp, int percent) {

	}

	@Override
	public boolean onInfo(MediaPlayer mp, int what, int extra) {
		switch (what) {
			case MediaPlayer.MEDIA_INFO_BUFFERING_START:
				if (mVideoView.isPlaying()) {
					mVideoView.pause();
					processDialog.show();
				}
				break;
			case MediaPlayer.MEDIA_INFO_BUFFERING_END:
				mVideoView.start();
				processDialog.dismiss();
				break;
			case MediaPlayer.MEDIA_INFO_DOWNLOAD_RATE_CHANGED:
				break;
		}
		return true;
	}
}
