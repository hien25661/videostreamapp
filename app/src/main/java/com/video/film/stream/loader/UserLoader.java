package com.video.film.stream.loader;

import android.app.Application;
import android.util.Log;

import com.video.film.stream.ApolloApplication;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.models.Profile;
import com.video.film.stream.models.WatchList;
import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.tvshows.Episode;
import com.video.film.stream.models.tvshows.Seasons;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.services.ApolloService;
import com.video.film.stream.services.reponses.LoginResponse;
import com.video.film.stream.services.reponses.movies.CheckTokenResponse;
import com.video.film.stream.services.reponses.movies.MovieLinkResponse;
import com.video.film.stream.utils.ParserJson;
import com.video.film.stream.utils.StringUtils;
import com.video.film.stream.utils.Utils;

import org.json.JSONException;

import java.util.ArrayList;

import javax.net.ssl.HttpsURLConnection;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nguyenvanhien on 6/30/17.
 */

public class UserLoader {
    static UserLoader instance;

    public static UserLoader getInstance() {
        if (instance == null) {
            instance = new UserLoader();
        }
        return instance;
    }

    public void login(String userName, String passWord, final SimpleCallBack callBack) {
        ApolloService.createServiceApi().getToken(userName, passWord)
                .enqueue(new Callback<LoginResponse>() {
                    @Override
                    public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                        if (response != null && response.body() != null
                                && response.body() instanceof LoginResponse) {
                            LoginResponse value = response.body();
                            if (StringUtils.isNotEmpty(value.getToken())) {
                                ApolloApplication.getInstance().getAppSettings()
                                        .setApolloToken(response.body().getToken());
                                ApolloApplication.getInstance().getAppSettings()
                                        .setTokenExpire(response.body().getExpire());
                                callBack.success();
                            } else {
                                callBack.failed();
                            }
                        } else {
                            callBack.failed();
                        }
                    }

                    @Override
                    public void onFailure(Call<LoginResponse> call, Throwable t) {
                        callBack.failed();
                    }
                });
    }

    public void getDetailMovie(Movie movie, SimpleCallBack callBack) {
        //ApolloService.createService().getToken()
    }

    public void playMovie(Movie movie, final SimpleCallBack callBack) {
        ApolloService.createServiceApi().playMovie(movie.getImdb(), "1", ApolloApplication.getInstance().getAppSettings().getApolloToken())
                .enqueue(new Callback<MovieLinkResponse>() {
                    @Override
                    public void onResponse(Call<MovieLinkResponse> call, Response<MovieLinkResponse> response) {
                        if (response != null && response.body() != null) {
                            if (StringUtils.isNotEmpty(response.body().getLink())) {
                                String url = Utils.getLinkMovie(response.body().getLink());
                                if (StringUtils.isNotEmpty(url)) {
                                    callBack.success(url);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MovieLinkResponse> call, Throwable t) {
                        callBack.failed();
                    }
                });
    }

    public void playTvShow(TvShow movie, String season, String episode, final SimpleCallBack callBack) {
        ApolloService.createServiceApi().playTvShow(movie.getImdb(),
                season,
                episode
                , "1"
                , ApolloApplication.getInstance().getAppSettings().getApolloToken())
                .enqueue(new Callback<MovieLinkResponse>() {
                    @Override
                    public void onResponse(Call<MovieLinkResponse> call, Response<MovieLinkResponse> response) {
                        if (response != null && response.body() != null) {
                            if (StringUtils.isNotEmpty(response.body().getLink())) {
                                String url = Utils.getLinkMovie(response.body().getLink());
                                if (StringUtils.isNotEmpty(url)) {
                                    callBack.success(url);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MovieLinkResponse> call, Throwable t) {
                        callBack.failed();
                    }
                });
    }

    public void playTvChannel(TvChanel movie, final SimpleCallBack callBack) {
        ApolloService.createServiceApi().playTvChannel(movie.getId()
                , "1"
                , ApolloApplication.getInstance().getAppSettings().getApolloToken())
                .enqueue(new Callback<MovieLinkResponse>() {
                    @Override
                    public void onResponse(Call<MovieLinkResponse> call, Response<MovieLinkResponse> response) {
                        if (response != null && response.body() != null) {
                            if (StringUtils.isNotEmpty(response.body().getLink())) {
                                String url = response.body().getLink();
                                if (StringUtils.isNotEmpty(url)) {
                                    callBack.success(url);
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<MovieLinkResponse> call, Throwable t) {
                        callBack.failed();
                    }
                });
    }

    public void getDetailTvShow(String imdb, final SimpleCallBack callBack) {
        String url = ApolloService.APOLLO_BASE_URL + "/info/" + imdb + ".json";
        ApolloService.createServiceString().getTvShowDetail(url).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                ArrayList<Seasons> infoSeasonList = new ArrayList<Seasons>();
                try {
                    infoSeasonList = ParserJson.getSeasonEpisodeList(response.body().toString());
                    callBack.success(infoSeasonList);
                } catch (JSONException e) {
                    callBack.failed();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                callBack.failed();
            }
        });

    }

    public void checkToken(String token, final SimpleCallBack callBack) {
        ApolloService.createServiceApi().checkToken(token).enqueue(new Callback<CheckTokenResponse>() {
            @Override
            public void onResponse(Call<CheckTokenResponse> call, Response<CheckTokenResponse> response) {
                if (response != null && response.body() != null && response.body() instanceof
                        CheckTokenResponse) {
                    CheckTokenResponse tokenResponse = response.body();
                    if (tokenResponse.getResult().equals("ok")) {
                        callBack.success();
                    } else {
                        callBack.failed();
                    }
                } else callBack.failed();
            }

            @Override
            public void onFailure(Call<CheckTokenResponse> call, Throwable t) {
                callBack.failed();
            }
        });
    }

    public void getWatchList(final SimpleCallBack callBack) {
        String url = "http://api.apollogroup.tv/watchlist/";
        ApolloService.createServiceString().getWatchList(url,
                ApolloApplication.getInstance().getAppSettings().getApolloToken()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                ArrayList<Movie> infoImdbList = new ArrayList<Movie>();
                try {
                    WatchList watchList = ParserJson.getWatchListMovie(response.body().toString());
                    if (watchList != null) {
                        if (watchList.getMovies() != null && watchList.getTvChanels() != null) {
                            infoImdbList = watchList.getMovies();
                            callBack.success(infoImdbList, watchList.getTvChanels());
                        } else {
                            callBack.success(new ArrayList<String>(),
                                    new ArrayList<String>());
                        }
                    } else {
                        callBack.success(new ArrayList<String>(),
                                new ArrayList<String>());
                    }
                } catch (JSONException e) {
                    callBack.success(new ArrayList<String>(),
                            new ArrayList<String>());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                callBack.failed();
            }
        });

    }

    public void addWatchListImdb(String imdbId, final SimpleCallBack callBack) {
        String url = "http://api.apollogroup.tv/watchlist/?watchlist=add&imdb=" + imdbId;
        ApolloService.createServiceString().addWatchListImdb(url,
                ApolloApplication.getInstance().getAppSettings().getApolloToken()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                ArrayList<Movie> infoImdbList = new ArrayList<Movie>();
                try {
                    WatchList watchList = ParserJson.getWatchListMovie(response.body().toString());
                    if (watchList != null) {
                        if (watchList.getMovies() != null && watchList.getTvChanels() != null) {
                            infoImdbList = watchList.getMovies();
                            callBack.success(infoImdbList, watchList.getTvChanels());
                            ApolloApplication.getInstance().getAppSettings()
                                    .addWatchListWhenLoad(infoImdbList, watchList.getTvChanels());
                        } else {
                            callBack.success(new ArrayList<String>(),
                                    new ArrayList<String>());
                        }
                    } else {
                        callBack.success(new ArrayList<String>(),
                                new ArrayList<String>());
                    }
                } catch (JSONException e) {
                    callBack.success(new ArrayList<String>(),
                            new ArrayList<String>());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                callBack.failed();
            }
        });

    }

    public void removeWatchListImdb(String imdbId, final SimpleCallBack callBack) {
        String url = "http://api.apollogroup.tv/watchlist/?watchlist=remove&imdb=" + imdbId;
        ApolloService.createServiceString().removeWatchList(url,
                ApolloApplication.getInstance().getAppSettings().getApolloToken()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                ArrayList<Movie> infoImdbList = new ArrayList<Movie>();
                try {
                    WatchList watchList = ParserJson.getWatchListMovie(response.body().toString());
                    if (watchList != null) {
                        if (watchList.getMovies() != null && watchList.getTvChanels() != null) {
                            infoImdbList = watchList.getMovies();
                            callBack.success(infoImdbList, watchList.getTvChanels());
                            ApolloApplication.getInstance().getAppSettings()
                                    .addWatchListWhenLoad(infoImdbList, watchList.getTvChanels());
                        } else {
                            callBack.success(new ArrayList<String>(),
                                    new ArrayList<String>());
                        }
                    } else {
                        callBack.success(new ArrayList<String>(),
                                new ArrayList<String>());
                    }
                } catch (JSONException e) {
                    callBack.success(new ArrayList<String>(),
                            new ArrayList<String>());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                callBack.failed();
            }
        });

    }

    public void addWatchListChannel(String channelID, final SimpleCallBack callBack) {
        String url = "http://api.apollogroup.tv/watchlist/?watchlist=add&channel=" + channelID;
        ApolloService.createServiceString().addWatchListChannel(url,
                ApolloApplication.getInstance().getAppSettings().getApolloToken()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                ArrayList<Movie> infoImdbList = new ArrayList<Movie>();
                try {
                    WatchList watchList = ParserJson.getWatchListMovie(response.body().toString());
                    if (watchList != null) {
                        if (watchList.getMovies() != null && watchList.getTvChanels() != null) {
                            infoImdbList = watchList.getMovies();
                            callBack.success(infoImdbList, watchList.getTvChanels());
                            ApolloApplication.getInstance().getAppSettings()
                                    .addWatchListWhenLoad(infoImdbList, watchList.getTvChanels());
                        } else {
                            callBack.success(new ArrayList<String>(),
                                    new ArrayList<String>());
                        }
                    } else {
                        callBack.success(new ArrayList<String>(),
                                new ArrayList<String>());
                    }

                } catch (JSONException e) {
                    callBack.success(new ArrayList<String>(),
                            new ArrayList<String>());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                callBack.failed();
            }
        });

    }

    public void removeWatchListChannel(String channelID, final SimpleCallBack callBack) {
        String url = "http://api.apollogroup.tv/watchlist/?watchlist=remove&channel=" + channelID;
        ApolloService.createServiceString().addWatchListChannel(url,
                ApolloApplication.getInstance().getAppSettings().getApolloToken()).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                ArrayList<Movie> infoImdbList = new ArrayList<Movie>();
                try {
                    WatchList watchList = ParserJson.getWatchListMovie(response.body().toString());
                    if (watchList != null) {
                        if (watchList.getMovies() != null && watchList.getTvChanels() != null) {
                            infoImdbList = watchList.getMovies();
                            callBack.success(infoImdbList, watchList.getTvChanels());
                            ApolloApplication.getInstance().getAppSettings()
                                    .addWatchListWhenLoad(infoImdbList, watchList.getTvChanels());
                        } else {
                            callBack.success(new ArrayList<String>(),
                                    new ArrayList<String>());
                        }
                    } else {
                        callBack.success(new ArrayList<String>(),
                                new ArrayList<String>());
                    }
                } catch (JSONException e) {
                    callBack.success(new ArrayList<String>(),
                            new ArrayList<String>());
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                callBack.failed();
            }
        });

    }

    public void loadProfile(final SimpleCallBack callBack) {
        String url = "http://api.apollogroup.tv/account/?token="
                + ApolloApplication.getInstance().getAppSettings().getApolloToken();
        ApolloService.createService().getInfoProfile(url).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                if (response != null && response.body() != null
                        && response.body() instanceof Profile) {
                    callBack.success(response.body());
                } else {
                    callBack.failed();
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                callBack.failed();
            }
        });
    }
}
