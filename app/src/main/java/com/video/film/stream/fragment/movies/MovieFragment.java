package com.video.film.stream.fragment.movies;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.video.film.stream.R;
import com.video.film.stream.adapters.movies.MovieListAdapter;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.fragment.tvshows.TvShowFragment;
import com.video.film.stream.loader.MovieLoader;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.movies.MovieCategory;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.ViewType;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class MovieFragment extends Fragment {
    @Bind(R.id.rcvMovie)
    UltimateRecyclerView rcvMovie;

    private Context mContext;
    private MovieCategory movieCategory;
    private MovieListAdapter movieListAdapter;
    private MovieLoader movieLoader;
    private ArrayList<Movie> movies;
    private boolean isFirstTime = true;

    public MovieFragment(){

    }
    public static MovieFragment newInstance(MovieCategory movieCate) {
        MovieFragment fragment = new MovieFragment();
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(Consts.CATEGORY, movieCate);
        fragment.setArguments(bundle);
        return fragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        movieLoader = MovieLoader.getInstance();
        if(movieListAdapter == null && isFirstTime) {
            loadData();
            isFirstTime = false;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        ButterKnife.bind(this,view);
        mContext = view.getContext();
        movieLoader = MovieLoader.getInstance();
        if(movieListAdapter == null) {
            loadData();
        }else {
            initView();
            rcvMovie.setAdapter(movieListAdapter);
        }
        return view;
    }

    private void initView() {
        rcvMovie.setHasFixedSize(true);
        int valueSpant = 2;
        if(Utils.isGoogleTV(mContext)){
            valueSpant = 4;
        }
        StaggeredGridLayoutManager layoutManager = new StaggeredGridLayoutManager(valueSpant,
                StaggeredGridLayoutManager.VERTICAL);
        rcvMovie.setLayoutManager(layoutManager);
    }

    private void loadData() {
        movieCategory = getArguments().getParcelable(Consts.CATEGORY);
        if(movieCategory!=null) {
            movieLoader.getMovieList(ViewType.MOVIE.getType(), movieCategory.getId(), new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    Log.e("VOA1", "" + movieCategory.getId());
                    movies = (ArrayList<Movie>) params[0];
                    setAdapterMovie(movies);
                }

                @Override
                public void failed() {

                }
            });
        }
    }

    private void setAdapterMovie(ArrayList<Movie> movies) {
        initView();
        movieListAdapter = new MovieListAdapter(movies);
        rcvMovie.setAdapter(movieListAdapter);
    }
    @Override
    public void onDestroy() {
        recycleFragmentView();
        super.onDestroy();
    }

    private void recycleFragmentView(){
        try{
            Glide.get(mContext).getBitmapPool().clearMemory();
            Glide.get(mContext).clearMemory();
        }catch (Exception ex){

        }
    }
}
