package com.video.film.stream.models.sub;

import com.video.film.stream.models.BaseModel;

/**
 * Created by nguyenvanhien on 7/10/17.
 */

public class SubTitle extends BaseModel {
    private String countryName;
    private String subUrl;
    private boolean isHeader = false;

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getSubUrl() {
        return subUrl;
    }

    public void setSubUrl(String subUrl) {
        this.subUrl = subUrl;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }
}
