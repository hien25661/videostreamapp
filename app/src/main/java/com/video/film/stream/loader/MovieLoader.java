package com.video.film.stream.loader;

import android.util.Log;

import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.models.sub.SubTitle;
import com.video.film.stream.services.ApolloService;
import com.video.film.stream.services.reponses.movies.LiveTvListResponse;
import com.video.film.stream.services.reponses.movies.MovieCategoryResponse;
import com.video.film.stream.services.reponses.movies.MovieListResponse;
import com.video.film.stream.services.reponses.movies.TvShowListResponse;
import com.video.film.stream.utils.AppSettings;
import com.video.film.stream.utils.ParserJson;

import org.json.JSONException;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class MovieLoader {
    static MovieLoader instance;

    public static MovieLoader getInstance() {
        if (instance == null) {
            instance = new MovieLoader();
        }
        return instance;
    }

    public void getCategoriesMovieList(String type, final SimpleCallBack callBack) {
        ApolloService.createService().getCategoriesMovieList(type).enqueue(new Callback<MovieCategoryResponse>() {
            @Override
            public void onResponse(Call<MovieCategoryResponse> call,
                                   Response<MovieCategoryResponse> response) {
                if (response != null && response.body() != null
                        && response.body() instanceof MovieCategoryResponse
                        && response.body().getMovieCategory() != null) {
                    callBack.success(response.body().getMovieCategory());
                } else {
                    callBack.failed();
                }
            }

            @Override
            public void onFailure(Call<MovieCategoryResponse> call, Throwable t) {
                callBack.failed();
            }
        });
    }

    public synchronized void getMovieList(String type, String categoryId, final SimpleCallBack callBack) {
        ApolloService.createService().getMovieList(type, categoryId).enqueue(new Callback<MovieListResponse>() {
            @Override
            public void onResponse(Call<MovieListResponse> call,
                                   Response<MovieListResponse> response) {
                if (response != null && response.body() != null
                        && response.body() instanceof MovieListResponse
                        && response.body().getMovieList() != null) {
                    callBack.success(response.body().getMovieList());
                } else {
                    callBack.failed();
                }
            }

            @Override
            public void onFailure(Call<MovieListResponse> call, Throwable t) {
                callBack.failed();
            }
        });
    }

    public synchronized void getTvShowList(String type, String categoryId, final SimpleCallBack callBack) {
        ApolloService.createService().getTvShowList(type, categoryId).enqueue(new Callback<TvShowListResponse>() {
            @Override
            public void onResponse(Call<TvShowListResponse> call,
                                   Response<TvShowListResponse> response) {
                if (response != null && response.body() != null
                        && response.body() instanceof TvShowListResponse
                        && response.body().getTvShowList() != null) {
                    callBack.success(response.body().getTvShowList());
                    Log.e("VVVVV", "" + response.body().toJson());
                } else {
                    callBack.failed();
                }
            }

            @Override
            public void onFailure(Call<TvShowListResponse> call, Throwable t) {
                Log.e("VVVVV", "" + t.getMessage());
                callBack.failed();
            }
        });
    }

    public synchronized void getLiveTvList(String type, String categoryId, final SimpleCallBack callBack) {
        ApolloService.createService().getLiveTvList(type, categoryId).enqueue(new Callback<LiveTvListResponse>() {
            @Override
            public void onResponse(Call<LiveTvListResponse> call,
                                   Response<LiveTvListResponse> response) {
                if (response != null && response.body() != null
                        && response.body() instanceof LiveTvListResponse
                        && response.body().getLiveTvList() != null) {
                    callBack.success(response.body().getLiveTvList());
                    Log.e("VVVVV", "" + response.body().toJson());
                } else {
                    callBack.failed();
                }
            }

            @Override
            public void onFailure(Call<LiveTvListResponse> call, Throwable t) {
                Log.e("VVVVV", "" + t.getMessage());
                callBack.failed();
            }
        });
    }

    public synchronized void searchMovie(String type, String searchText, final SimpleCallBack callBack) {
        String url = "http://json.apollogroup.tv/search.php?type=movie&search=" + searchText;
        ApolloService.createService().searchMovie(url).enqueue(new Callback<MovieListResponse>() {
            @Override
            public void onResponse(Call<MovieListResponse> call, Response<MovieListResponse> response) {
                if (response != null && response.body() != null
                        && response.body() instanceof MovieListResponse) {
                    callBack.success(response.body().getMovieList());
                } else {
                    callBack.failed();
                }
            }

            @Override
            public void onFailure(Call<MovieListResponse> call, Throwable t) {
                callBack.failed();
            }
        });
    }

    public synchronized void searchTvShow(String type, String searchText, final SimpleCallBack callBack) {
        String url = "http://json.apollogroup.tv/search.php?type=tv&search=" + searchText;
        ApolloService.createService().searchTvShow(url).enqueue(new Callback<TvShowListResponse>() {
            @Override
            public void onResponse(Call<TvShowListResponse> call, Response<TvShowListResponse> response) {
                if (response != null && response.body() != null
                        && response.body() instanceof TvShowListResponse) {
                    callBack.success(response.body().getTvShowList());
                } else {
                    callBack.failed();
                }
            }

            @Override
            public void onFailure(Call<TvShowListResponse> call, Throwable t) {
                callBack.failed();
            }
        });
    }

    public synchronized void searchLiveTv(String type, String searchText, final SimpleCallBack callBack) {
        String url = "http://json.apollogroup.tv/search.php?type=livetv&search=" + searchText;
        ApolloService.createService().searchLiveTv(url).enqueue(new Callback<LiveTvListResponse>() {
            @Override
            public void onResponse(Call<LiveTvListResponse> call, Response<LiveTvListResponse> response) {
                if (response != null && response.body() != null
                        && response.body() instanceof LiveTvListResponse) {
                    callBack.success(response.body().getLiveTvList());
                } else {
                    callBack.failed();
                }
            }

            @Override
            public void onFailure(Call<LiveTvListResponse> call, Throwable t) {
                callBack.failed();
            }
        });
    }

    public synchronized void searchSubTitle(String imdb, final SimpleCallBack callBack) {
        String url = "http://subs.apollogroup.tv/?imdb=" + imdb;
        ApolloService.createServiceString().searchSubTitle(url)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response != null && response.body() != null) {
                            ArrayList<SubTitle> list = new ArrayList<SubTitle>();
                            try {
                                list = ParserJson.getSubTitleList(response.body().toString());
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            callBack.success(list);
                        } else {
                            callBack.failed();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        callBack.failed();
                    }
                });

    }

    public synchronized void searchSubTitleTvShow(String imdb, final String season, final String episode, final SimpleCallBack callBack) {
        String url = "http://subs.apollogroup.tv/?imdb=" + imdb;
        ApolloService.createServiceString().searchSubTitle(url)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response != null && response.body() != null) {
                            ArrayList<SubTitle> list = new ArrayList<SubTitle>();
                            try {
                                list = ParserJson.getSubTitleListTvShow(response.body().toString(),
                                        season,episode);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            callBack.success(list);
                        } else {
                            callBack.failed();
                        }
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        callBack.failed();
                    }
                });

    }
}
