package com.video.film.stream.customview.tvshow;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;

import com.video.film.stream.R;
import com.video.film.stream.adapters.tvshows.EpisodeListAdapter;
import com.video.film.stream.callbacks.CallBack;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.models.tvshows.Episode;
import com.video.film.stream.models.tvshows.Seasons;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static android.content.Context.LAYOUT_INFLATER_SERVICE;

/**
 * Created by nguyenvanhien on 7/3/17.
 */

public class SeasonView extends LinearLayout {
    Context mContext;
    @Bind(R.id.rcvEpisode)
    RecyclerView rcvEpisode;

    ArrayList<Episode> listEpisode = new ArrayList<>();
    EpisodeListAdapter episodeAdapter;
    private SelectorListenerObject callback;

    public SeasonView(Context context) {
        super(context);
        mContext = context;
        init();
    }

    public SeasonView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mContext = context;
        init();
    }

    public SeasonView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mContext = context;
        init();
    }

    private void init() {
        View v = inflate(getContext(), R.layout.layout_season, this);
        ButterKnife.bind(this, v);
        initView();

    }

    private void initView() {
        LinearLayoutManager layoutManagager = new LinearLayoutManager(mContext);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext,2);
        rcvEpisode.setHasFixedSize(false);
        if(Utils.isGoogleTV(mContext)){
            rcvEpisode.setLayoutManager(gridLayoutManager);
        }else {
            rcvEpisode.setLayoutManager(layoutManagager);
        }
       
    }


    public void processData(Seasons season) {
        if(season!=null) {
            chooseSeason(season);
        }
    }
    public void setCallBackSelectedEpisode(SelectorListenerObject callback){
        this.callback = callback;
    }

    public void chooseSeason(Seasons season){
        if(season!=null){
            listEpisode = season.getEpisodes();
            episodeAdapter = new EpisodeListAdapter(listEpisode, callback);
            rcvEpisode.setAdapter(episodeAdapter);
        }
    }

}
