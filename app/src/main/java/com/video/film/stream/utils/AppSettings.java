package com.video.film.stream.utils;

import android.content.Context;

import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.movies.Movie;

import java.io.IOException;
import java.nio.channels.Channel;
import java.util.ArrayList;

/**
 * Created by hien.nv on 5/4/17.
 */

public class AppSettings extends SharedPreferencesService {
    public Context mContext;

    public AppSettings(Context context) {
        super(context, context.getPackageName());
        mContext = context;
    }


    public void setApolloToken(String token) {
        this.getSettings().edit().putString("ApolloToken", token).apply();
    }

    public String getApolloToken() {
        return getSettings().getString("ApolloToken", "");
    }

    public void setTokenExpire(long time) {
        this.getSettings().edit().putLong("TokenExpire", time).apply();
    }

    public long getTokenExpire() {
        return getSettings().getLong("TokenExpire", 0);
    }

    public ArrayList<String> getIbdbWatchList() {
        ArrayList<String> listImdb = new ArrayList<>();
        try {
            listImdb = (ArrayList<String>) ObjectSerializer.deserialize
                    (this.getSettings().getString("imdbWatchList",
                            ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
        }
        return listImdb;
    }

    public void addImdbToWatchList(String imdb) {
        ArrayList<String> imdbWatchList = getIbdbWatchList();
        if (!imdbWatchList.contains(imdb)) {
            imdbWatchList.add(imdb);
        }
        try {
            this.getSettings().edit().putString("imdbWatchList",
                    ObjectSerializer.serialize(imdbWatchList)).apply();
        } catch (IOException e) {
        }

    }

    public ArrayList<String> getChannelWatchList() {
        ArrayList<String> listImdb = new ArrayList<>();
        try {
            listImdb = (ArrayList<String>) ObjectSerializer.deserialize
                    (this.getSettings().getString("channelWatchList",
                            ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
        }
        return listImdb;
    }

    public void addChannelToWatchList(String imdb) {
        ArrayList<String> imdbWatchList = getIbdbWatchList();
        if (!imdbWatchList.contains(imdb)) {
            imdbWatchList.add(imdb);
        }
        try {
            this.getSettings().edit().putString("channelWatchList",
                    ObjectSerializer.serialize(imdbWatchList)).apply();
        } catch (IOException e) {
        }

    }

    public void addWatchListWhenLoad(ArrayList<Movie> movies, ArrayList<TvChanel> channels) {
        ArrayList<String> imdbList = new ArrayList<>();
        ArrayList<String> channelList = new ArrayList<>();
        for (Movie item : movies) {
            imdbList.add(item.getImdb());
        }
        for(TvChanel tvChanel : channels){
            channelList.add(tvChanel.getId());
        }
        try {
            this.getSettings().edit().putString("imdbWatchList",
                    ObjectSerializer.serialize(imdbList)).apply();

            this.getSettings().edit().putString("channelWatchList",
                    ObjectSerializer.serialize(channelList)).apply();
        } catch (IOException e) {
        }

    }

    public boolean isImdbBelongWatchList(String imdb){
        ArrayList<String> movies = getIbdbWatchList();
        if(movies.contains(imdb)) return true;
        return false;
    }

    public boolean isChannelBelongWatchList(String imdb){
        ArrayList<String> movies = getChannelWatchList();
        if(movies.contains(imdb)) return true;
        return false;
    }



}
