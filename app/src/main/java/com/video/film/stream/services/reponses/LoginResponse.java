package com.video.film.stream.services.reponses;

import com.video.film.stream.models.BaseModel;

/**
 * Created by nguyenvanhien on 6/30/17.
 */

public class LoginResponse extends BaseModel {
    private String token;
    private long expire;

    public String getToken() {
        return token;
    }

    public long getExpire() {
        return expire;
    }
}
