package com.video.film.stream.services.reponses.movies;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.video.film.stream.models.BaseModel;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.movies.MovieCategory;

import java.util.ArrayList;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class MovieListResponse extends BaseModel {
    @SerializedName("result")
    @Expose
    private ArrayList<Movie> movieList = new ArrayList<>();

    public ArrayList<Movie> getMovieList() {
        return movieList;
    }

}
