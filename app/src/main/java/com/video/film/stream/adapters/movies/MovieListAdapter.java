package com.video.film.stream.adapters.movies;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.video.film.stream.R;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.dialog.DialogInfoMovies;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.WatchListAction;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class MovieListAdapter extends UltimateViewAdapter<MovieListAdapter.ViewHolder> {
    private ArrayList<Movie> movies;
    private Context mContext;
    private boolean isWatchList = false;

    public MovieListAdapter(ArrayList<Movie> movies) {
        this.movies = movies;
    }

    public MovieListAdapter(ArrayList<Movie> movies, boolean isWatchList) {
        this.movies = movies;
        this.isWatchList = isWatchList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);
        ViewHolder vh = new ViewHolder(v);
        mContext = v.getContext();
        return vh;
    }

    @Override
    public ViewHolder newFooterHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder newHeaderHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (position >= 0 && position < movies.size()) {
            final Movie movie = movies.get(position);
            if (movie != null) {
                Utils.showImageFromUrlIntoView(mContext, movie.getPoster(), holder.imvFilm, position);
                holder.imvFilm.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        DialogInfoMovies dialogInfo = new DialogInfoMovies(mContext);
                        dialogInfo.setMovie(movie);
                        dialogInfo.setDeleteCallback(new SelectorListenerObject() {
                            @Override
                            public void onSelected(Object... params) {
                                if(isWatchList){
                                    if(params[0]!=null){
                                        if(params[0] == WatchListAction.ADD){
                                            movies.add(movie);
                                            notifyDataSetChanged();
                                        }else {
                                            if(params[0] == WatchListAction.DELETE){
                                                Log.e("AAAA","DELETE");
                                                movies.remove(movie);
                                                notifyDataSetChanged();
                                            }
                                        }
                                    }
                                }
                            }
                        });
                        dialogInfo.show();
                    }
                });
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getAdapterItemCount() {
        return movies.size();
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imvFilm)
        ImageView imvFilm;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        try {
            if (holder != null) {
                Glide.clear(holder.imvFilm);
            }
        } catch (Exception ex) {
        }
        super.onViewRecycled(holder);
    }
}
