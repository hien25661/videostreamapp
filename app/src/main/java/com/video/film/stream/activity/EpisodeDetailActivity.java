package com.video.film.stream.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.video.film.stream.ApolloApplication;
import com.video.film.stream.BaseActivity;
import com.video.film.stream.R;
import com.video.film.stream.adapters.tvshows.SeasonDialogAdapter;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.customview.tvshow.SeasonView;
import com.video.film.stream.dialog.DialogChooseSub;
import com.video.film.stream.dialog.ProcessDialog;
import com.video.film.stream.loader.MovieLoader;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.sub.SubTitle;
import com.video.film.stream.models.tvshows.Episode;
import com.video.film.stream.models.tvshows.Seasons;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.player.FullScreenVideoPlayerActivity;
import com.video.film.stream.player.VideoPlayerActivity;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.StringUtils;
import com.video.film.stream.utils.Utils;
//import com.wass08.vlcsimpleplayer.FullscreenVlcPlayer;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyenvanhien on 7/1/17.
 */

public class EpisodeDetailActivity extends BaseActivity {
    @Bind(R.id.imvPoster)
    ImageView imvPoster;
    /*    @Bind(R.id.tvTitle)
        CustomTextView tvTitle;
        @Bind(R.id.tvInfo)
        CustomTextView tvInfo;*/
    @Bind(R.id.tvNameScreen)
    CustomTextView tvNameScreen;

    @Bind(R.id.tvSynopis)
    CustomTextView tvSynopis;

    @Bind(R.id.tvTitle)
    CustomTextView tvTitle;
    @Bind(R.id.tvSeason)
    CustomTextView tvSeason;
    @Bind(R.id.tvInfo)
    CustomTextView tvInfo;

    @Bind(R.id.seasonview)
    SeasonView seasonView;

    @Bind(R.id.scvParent)
    NestedScrollView scvParent;
    @Bind(R.id.rcvAllSeason)
    RecyclerView rcvAllSeason;

    private TvShow tvShow;
    private UserLoader userLoader;
    private ProcessDialog processDialog;
    private LoginDialog loginDialog;
    private Episode currentEpisode;
    private Seasons currentSeasons;
    private int currentIndexSeason = 0;
    private DialogChooseSub dialogChooseSub;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_episode);
        ButterKnife.bind(this);
        userLoader = UserLoader.getInstance();
        processDialog = new ProcessDialog(this);
        loginDialog = new LoginDialog(this, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                playMovie();
            }

            @Override
            public void failed() {

            }
        });
        initData();
    }

    private void initData() {
        tvShow = getIntent().getParcelableExtra(Consts.MOVIE);
        currentEpisode = getIntent().getParcelableExtra(Consts.EPISODE);
        currentIndexSeason = getIntent().getIntExtra("season_pos", 0);
        if (tvShow != null && currentEpisode != null) {
            tvNameScreen.setText(tvShow.getTitle());
            loadInfoDetail(tvShow, currentEpisode);
            loadEpisode(currentIndexSeason);
            loadAllSeasonView();
        }

    }


    private void loadAllSeasonView() {
        if (tvShow.getSeasonDetailList() != null && tvShow.getSeasonDetailList().size() > 0) {
            if(Utils.isGoogleTV(this)){
                rcvAllSeason.setLayoutManager(new GridLayoutManager(this,3));
            } else {
                rcvAllSeason.setLayoutManager(new LinearLayoutManager(this));
            }
            rcvAllSeason.setHasFixedSize(false);
            SeasonDialogAdapter allSeasonAdapter = new SeasonDialogAdapter(
                    tvShow.getSeasonDetailList(),
                    1,
                    new SelectorListenerObject() {
                        @Override
                        public void onSelected(Object... params) {
                            if (params[0] != null && params[0] instanceof Seasons) {
                                Seasons item = (Seasons) params[0];
                                if (item != null) {
                                    setSeasonName(item);
                                }
                            }
                            if (params[1] != null && params[1] instanceof Integer) {
                                currentIndexSeason = (int) params[1];
                            }
                        }
                    }
                    , currentIndexSeason);
            rcvAllSeason.setAdapter(allSeasonAdapter);
        }
    }


    private void loadInfoDetail(TvShow movie, Episode currentEpisode) {
        Utils.showImageFromUrlIntoView(this, currentEpisode.getScreenshot(), imvPoster, 0);
       /* tvTitle.setText(movie.getTitle());
        tvInfo.setText(movie.getGenres() + " - " + movie.getTimeRelease());*/
        tvSynopis.setText(currentEpisode.getPlot());
        tvTitle.setText(currentEpisode.getTitle());

        Seasons season = tvShow.getSeasonDetailList().get(currentIndexSeason);
        if (season != null) {
            tvSeason.setText("SEASON " + season.getSeasonName() + " EP " + currentEpisode.getName());
        }
        tvInfo.setText("Episode Aired " + Utils.getDateFromSecondEpisode(currentEpisode.getReleased()));

        scvParent.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                // Ready, move up
                scvParent.scrollTo(0, 0);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    scvParent.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    scvParent.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });
        scvParent.requestChildFocus(imvPoster, imvPoster);
    }

    private void loadEpisode(int index) {
        seasonView.setCallBackSelectedEpisode(new SelectorListenerObject() {
            @Override
            public void onSelected(Object... params) {
                if (params[0] != null && params[0] instanceof Episode) {
                    Episode episode = (Episode) params[0];
                    currentEpisode = episode;
                    loadInfoDetail(tvShow, currentEpisode);
                    loadEpisode(currentIndexSeason);
                }
            }
        });
        if (tvShow.getSeasonDetailList() != null && tvShow.getSeasonDetailList().size() > 0
                && index < tvShow.getSeasonDetailList().size()) {
            ArrayList<Seasons> listSeason = tvShow.getSeasonDetailList();
            setSeasonName(listSeason.get(index));
        }
    }

    private void setSeasonName(Seasons season) {
        if (season != null) {
            seasonView.processData(season);
        }
    }

    @OnClick(R.id.imvBack)
    public void onBack() {
        onBackPressed();
    }

    private void playMovie() {
        if (currentEpisode != null) {
            processDialog.show();
            MovieLoader movieLoader = MovieLoader.getInstance();
            Seasons seasons = tvShow.getSeasonDetailList().get(currentIndexSeason);
            if (seasons != null) {
                movieLoader.searchSubTitleTvShow(tvShow.getImdb(), seasons.getSeasonName(),
                        currentEpisode.getName(), new SimpleCallBack() {
                            @Override
                            public void success(Object... params) {
                                ArrayList<SubTitle> subList = new ArrayList<SubTitle>();
                                subList = (ArrayList<SubTitle>) params[0];
                                processDialog.dismiss();
                                if (subList != null && subList.size() > 0) {
                                    SubTitle header = new SubTitle();
                                    header.setHeader(true);
                                    header.setCountryName("None");
                                    subList.add(0, header);
                                    dialogChooseSub = new DialogChooseSub(
                                            EpisodeDetailActivity.this, subList, new SelectorListenerObject() {
                                        @Override
                                        public void onSelected(Object... params) {
                                            if (params[0] != null && params[0] instanceof SubTitle) {
                                                startMoviePlayer((SubTitle) params[0]);
                                            }
                                        }
                                    }
                                    );
                                    dialogChooseSub.show();
                                } else {
                                    startMoviePlayer(null);
                                }

                            }

                            @Override
                            public void failed() {
                                startMoviePlayer(null);
                            }
                        });
            }else {
                processDialog.dismiss();
            }
        }
    }


    private void startMoviePlayer(final SubTitle sub) {
        if (currentEpisode != null) {
            try {
                processDialog.show();
                Seasons seasons = tvShow.getSeasonDetailList().get(currentIndexSeason);
                if (seasons != null) {
                    userLoader.playTvShow(tvShow,
                            seasons.getSeasonName(),
                            currentEpisode.getName()
                            , new SimpleCallBack() {
                                @Override
                                public void success(Object... params) {
                                    processDialog.dismiss();
                                    if (params[0] != null && params[0] instanceof String) {
                                        tvShow.setLink((String) params[0]);
                                        /*Intent intent = new Intent(EpisodeDetailActivity.this,
                                                FullScreenVideoPlayerActivity.class);
                                        intent.putExtra(VideoPlayerActivity.TVSHOWS, tvShow);
                                        startActivity(intent);*/
                                        Intent intent = new Intent(EpisodeDetailActivity.this, VideoViewSubtitleNew.class);
                                        intent.putExtra("name", tvShow.getTitle());
                                        intent.putExtra("url", tvShow.getLink());
                                        if (sub != null) {
                                            if (StringUtils.isNotEmpty(sub.getSubUrl())) {
                                                intent.putExtra("sub", sub.getSubUrl());
                                            }
                                        }
                                        if(dialogChooseSub!=null && dialogChooseSub.isShowing()){
                                            dialogChooseSub.dismiss();
                                        }
                                        startActivity(intent);
                                    }
                                }

                                @Override
                                public void failed() {
                                    processDialog.dismiss();
                                }
                            });
                }
            } catch (Exception ex) {
                processDialog.dismiss();
            }
        }
    }

    @OnClick(R.id.imvPlay)
    public void playEpisode() {
        Utils.checkValidToken(ApolloApplication.getInstance().getAppSettings().getApolloToken(),
                new SimpleCallBack() {
                    @Override
                    public void success(Object... params) {
                        playMovie();
                    }

                    @Override
                    public void failed() {
                        loginDialog.show();
                    }
                });
    }

}
