package com.video.film.stream.models.tvshows;

import android.os.Parcel;
import android.os.Parcelable;

import com.video.film.stream.models.BaseModel;

/**
 * Created by nguyenvanhien on 7/2/17.
 */

public class Episode extends BaseModel implements Parcelable{
    private int season;
    private int episode;
    private String released;
    private String title;
    private String plot;
    private String screenshot;
    private String name;

    public Episode(){}
    protected Episode(Parcel in) {
        season = in.readInt();
        episode = in.readInt();
        released = in.readString();
        title = in.readString();
        plot = in.readString();
        screenshot = in.readString();
        name = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(season);
        dest.writeInt(episode);
        dest.writeString(released);
        dest.writeString(title);
        dest.writeString(plot);
        dest.writeString(screenshot);
        dest.writeString(name);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Episode> CREATOR = new Creator<Episode>() {
        @Override
        public Episode createFromParcel(Parcel in) {
            return new Episode(in);
        }

        @Override
        public Episode[] newArray(int size) {
            return new Episode[size];
        }
    };

    public int getSeason() {
        return season;
    }

    public int getEpisode() {
        return episode;
    }

    public String getReleased() {
        return released;
    }

    public String getTitle() {
        return title;
    }

    public String getPlot() {
        return plot;
    }

    public String getScreenshot() {
        return screenshot;
    }

    public void setSeason(int season) {
        this.season = season;
    }

    public void setEpisode(int episode) {
        this.episode = episode;
    }

    public void setReleased(String released) {
        this.released = released;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    public void setScreenshot(String screenshot) {
        this.screenshot = screenshot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
