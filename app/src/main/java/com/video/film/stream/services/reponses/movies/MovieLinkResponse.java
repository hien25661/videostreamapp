package com.video.film.stream.services.reponses.movies;

import com.video.film.stream.models.BaseModel;

/**
 * Created by nguyenvanhien on 7/1/17.
 */

public class MovieLinkResponse extends BaseModel {
    private String link;
    private String player;

    public String getLink() {
        return link;
    }

    public String getPlayer() {
        return player;
    }
}
