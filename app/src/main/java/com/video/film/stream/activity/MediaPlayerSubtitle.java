package com.video.film.stream.activity;

/**
 * Created by nguyenvanhien on 7/31/17.
 */

import java.io.File;
import java.io.IOException;

import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnPreparedListener;
import io.vov.vitamio.MediaPlayer.OnTimedTextListener;
import io.vov.vitamio.widget.MediaController;

import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.graphics.PixelFormat;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.widget.TextView;
import android.widget.Toast;

import com.video.film.stream.BaseActivity;
import com.video.film.stream.R;
import com.video.film.stream.callbacks.DownloadTask;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.dialog.ProcessDialog;
import com.video.film.stream.utils.StringUtils;
import com.video.film.stream.utils.Utils;

public class MediaPlayerSubtitle extends BaseActivity implements Callback, OnPreparedListener, OnTimedTextListener {

    SurfaceView splayer;
    SurfaceHolder sholder;
    TextView tv;
    private MediaPlayer mediaPlayer;
    private String path = "";
    private String subtitle_path = "";
    private ProcessDialog processDialog;

    private MediaController mediaController;

    File subFile;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!LibsChecker.checkVitamioLibs(this))
            return;
        setContentView(R.layout.subtitle1);
        tv = (TextView) findViewById(R.id.subtitle_view);
        splayer = (SurfaceView) findViewById(R.id.surface);
        sholder = splayer.getHolder();
        sholder.setFormat(PixelFormat.RGBA_8888);
        sholder.addCallback(this);
        path = getIntent().getStringExtra("url");
        subtitle_path = getIntent().getStringExtra("sub");
        subFile = new File(this.getFilesDir(),"a1.srt");

        mediaController = new MediaController(this);

        processDialog = new ProcessDialog(this);
        processDialog.show();

        if(StringUtils.isNotEmpty(subtitle_path)){
            final DownloadTask task = new DownloadTask(this);
            task.execute(subtitle_path.replaceAll("\"",""));
            task.setCallBackCompleted(new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    path = getIntent().getStringExtra("url");
                    if (StringUtils.isEmpty(path)) {
                        Toast.makeText(MediaPlayerSubtitle.this, "Cannot Play Video. Please Try again!", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        //playVideo();
                    }
                }

                @Override
                public void failed() {
                    path = getIntent().getStringExtra("url");
                    if (StringUtils.isEmpty(path)) {
                        Toast.makeText(MediaPlayerSubtitle.this, "Cannot Play Video. Please Try again!", Toast.LENGTH_LONG).show();
                        return;
                    } else {
                        //playVideo();
                    }
                }
            });
        }else {
            path = getIntent().getStringExtra("url");
            if (StringUtils.isEmpty(path)) {
                Toast.makeText(MediaPlayerSubtitle.this, "Cannot Play Video. Please Try again!", Toast.LENGTH_LONG).show();
                return;
            } else {
                //playVideo();
            }
        }
    }

    private void playVideo() {
        try {
            if (StringUtils.isEmpty(path)) {
                // Tell the user to provide an audio file URL.
                Toast.makeText(MediaPlayerSubtitle.this, "Cannot Play Video. Please Try again!", Toast.LENGTH_LONG).show();
                return;
            }
            mediaPlayer = new MediaPlayer(this);
            mediaPlayer.setDataSource(path);
            mediaPlayer.setDisplay(sholder);
            mediaPlayer.prepareAsync();
            mediaPlayer.setOnPreparedListener(this);
            mediaPlayer.setOnTimedTextListener(this);


            // TODO Auto-generated catch block
        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        // TODO Auto-generated method stub
        playVideo();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // TODO Auto-generated method stub

    }

    private void startVPback() {
        processDialog.dismiss();
        mediaPlayer.start();
    }

    @Override
    public void onPrepared(MediaPlayer arg0) {

        // TODO Auto-generated method stub
        startVPback();
        mediaPlayer.addTimedTextSource(subFile.getAbsolutePath());
        if(StringUtils.isNotEmpty(subtitle_path)) {
            mediaPlayer.setTimedTextShown(true);
        }else {
            mediaPlayer.setTimedTextShown(false);
        }

        mediaController.setMediaPlayer(new MediaController.MediaPlayerControl() {
            @Override
            public void start() {
                mediaPlayer.start();
            }

            @Override
            public void pause() {
                mediaPlayer.pause();
            }

            @Override
            public long getDuration() {
                try {
                return mediaPlayer.getDuration();
                }catch (Exception e){

                }
                return 0;
            }

            @Override
            public long getCurrentPosition() {
                try {
                    return mediaPlayer.getCurrentPosition();
                }catch (Exception e){

                }
                return 0;
            }

            @Override
            public void seekTo(long pos) {
                try {
                    mediaPlayer.seekTo(pos);
                }catch (Exception ex){}
            }

            @Override
            public boolean isPlaying() {
                try {
                    return mediaPlayer.isPlaying();
                }catch (Exception ex){}
                return false;
            }

            @Override
            public int getBufferPercentage() {
                return 0;
            }
        });
        mediaController.setAnchorView(splayer);
        mediaController.setEnabled(true);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                mediaController.show();
            }
        });


    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        relaMediaPlay();
    }

    private void relaMediaPlay() {
        // TODO Auto-generated method stub
        if (mediaPlayer != null) {
            mediaPlayer.release();
            mediaPlayer = null;
        }

    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
        relaMediaPlay();

    }

    @Override
    public void onTimedText(String text) {
        // TODO Auto-generated method stub
        tv.setText(text);
    }

    @Override
    public void onTimedTextUpdate(byte[] pixels, int width, int height) {
        // TODO Auto-generated method stub

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        mediaController.show();
        return false;
    }
}
