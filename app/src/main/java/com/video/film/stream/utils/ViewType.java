package com.video.film.stream.utils;

/**
 * Created by nguyenvanhien on 6/28/17.
 */

public enum ViewType {
    MOVIE("movie"), TVSHOW("tv"), TVCHANEL("livetv");
    String type;

    ViewType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }
}
