package com.video.film.stream.fragment;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.video.film.stream.R;
import com.video.film.stream.adapters.movies.MoviePagerAdapter;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.fragment.liveTv.LiveTvFragment;
import com.video.film.stream.fragment.tvshows.TvShowFragment;
import com.video.film.stream.loader.MovieLoader;
import com.video.film.stream.models.movies.MovieCategory;
import com.video.film.stream.utils.ViewType;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class LiveTvParentFragment extends Fragment {
    @Bind(R.id.vpr_movie)
    ViewPager vprMovie;
    @Bind(R.id.sliding_tabs)
    TabLayout tabLayout;

    MovieLoader movieLoader;
    private Context mContext;
    private MoviePagerAdapter moviewPagerAdapter;
    private ArrayList<MovieCategory> movieCategoryList = new ArrayList<>();
    private boolean isLoadedData = false;
    ArrayList<Fragment> listFragment = new ArrayList<>();
    ArrayList<String> listFragmentTitle = new ArrayList<>();
    public LiveTvParentFragment() {
    }

    public static LiveTvParentFragment newInstance() {
        LiveTvParentFragment fragment = new LiveTvParentFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_parent_movie, container, false);
        ButterKnife.bind(this, view);
        this.mContext = view.getContext();
        movieLoader = MovieLoader.getInstance();
        if(!isLoadedData) {
            loadData();
        }else {
            setDataPager(movieCategoryList);
        }
        return view;
    }

    private void loadData() {
        loadCategoryList();
    }

    private void loadCategoryList() {
        movieLoader.getCategoriesMovieList(ViewType.TVCHANEL.getType(),new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                movieCategoryList = (ArrayList<MovieCategory>) params[0];
                setDataPager(movieCategoryList);
                isLoadedData = true;
            }

            @Override
            public void failed() {

            }
        });
    }

    private void setDataPager(ArrayList<MovieCategory> movieCategoryList) {
        if(moviewPagerAdapter == null) {

            if (movieCategoryList != null && movieCategoryList.size() > 0) {
                for (int i = 0; i < movieCategoryList.size(); i++) {
                    MovieCategory movieCate = movieCategoryList.get(i);
                    LiveTvFragment fragment = LiveTvFragment.newInstance(movieCate);
                    listFragment.add(fragment);
                    listFragmentTitle.add(movieCate.getName());
                }
                if (!isAdded()) return;
                moviewPagerAdapter = new MoviePagerAdapter(getChildFragmentManager(), listFragment, listFragmentTitle);
                vprMovie.setAdapter(moviewPagerAdapter);
                //vprMovie.setOffscreenPageLimit(moviewPagerAdapter.getCount());
                tabLayout.setupWithViewPager(vprMovie);
                tabLayout.setTabTextColors(Color.WHITE, Color.parseColor("#e9cd26"));
                tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#e9cd26"));
            }
        }else {
            moviewPagerAdapter = new MoviePagerAdapter(getChildFragmentManager(), listFragment, listFragmentTitle);
            vprMovie.setAdapter(moviewPagerAdapter);
            //vprMovie.setOffscreenPageLimit(moviewPagerAdapter.getCount());
            tabLayout.setupWithViewPager(vprMovie);
            tabLayout.setTabTextColors(Color.WHITE, Color.parseColor("#e9cd26"));
            tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#e9cd26"));
        }
    }

    @Override
    public void onDestroy() {
        recycleFragmentView();
        super.onDestroy();
    }

    private void recycleFragmentView(){
        try{
            Glide.get(mContext).getBitmapPool().clearMemory();
            Glide.get(mContext).clearMemory();
        }catch (Exception ex){

        }
    }
}
