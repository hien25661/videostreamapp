package com.video.film.stream.models.tvshows;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by nguyenvanhien on 7/2/17.
 */

public class NextSeason implements Parcelable {
    private int season;
    private int episode;
    private String released;
    private String title;
    private String plot;
    private String screenshot;

    protected NextSeason(Parcel in) {
        season = in.readInt();
        episode = in.readInt();
        released = in.readString();
        title = in.readString();
        plot = in.readString();
        screenshot = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(season);
        dest.writeInt(episode);
        dest.writeString(released);
        dest.writeString(title);
        dest.writeString(plot);
        dest.writeString(screenshot);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<NextSeason> CREATOR = new Creator<NextSeason>() {
        @Override
        public NextSeason createFromParcel(Parcel in) {
            return new NextSeason(in);
        }

        @Override
        public NextSeason[] newArray(int size) {
            return new NextSeason[size];
        }
    };

    public int getSeason() {
        return season;
    }

    public int getEpisode() {
        return episode;
    }

    public String getReleased() {
        return released;
    }

    public String getTitle() {
        return title;
    }

    public String getPlot() {
        return plot;
    }

    public String getScreenshot() {
        return screenshot;
    }
}
