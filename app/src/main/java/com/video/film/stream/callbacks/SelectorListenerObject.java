package com.video.film.stream.callbacks;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public interface SelectorListenerObject {
    void onSelected(Object... params);
}
