package com.video.film.stream.utils;

import android.app.UiModeManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.text.Spannable;
import android.text.Spanned;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.video.film.stream.ApolloApplication;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.PlaceHolderDrawableHelper;
import com.video.film.stream.loader.UserLoader;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.content.Context.UI_MODE_SERVICE;

/**
 * Created by nguyenvanhien on 6/29/17.
 */

public class Utils {

    public static int getScreenWidth() {
        return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static void showImageFromUrlIntoView(Context context, String url, ImageView imv, int pos) {
        if (StringUtils.isNotEmpty(url)) {
            Glide.clear(imv);
            Glide.get(context).getBitmapPool().clearMemory();
            Glide.get(context).clearMemory();
            Glide.with(context).load(url)
                    .sizeMultiplier(0.7f)
                    .placeholder(PlaceHolderDrawableHelper.getBackgroundDrawable(pos))
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .fitCenter()
                    .into(imv);
        }
    }

    public static String getDateFromSecond(String second) {
        long mSecond = Long.parseLong(second);
        Date date = new Date(mSecond * 1000);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(mSecond * 1000L);

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        String strHour = "";

        strHour = hour + "h";

        DateFormat df = new SimpleDateFormat(" - dd MMM yyyy");//"2h - 14 May 2015"
        return strHour + df.format(date);
    }

    public static String getDateFromSecondSeason(String second) {
        long mSecond = Long.parseLong(second);
        Date date = new Date(mSecond * 1000);
        DateFormat df = new SimpleDateFormat("d MMM.yyyy");//"2h - 14 May 2015"
        return df.format(date);
    }

    public static String getDateFromSecondEpisode(String second) {
        long mSecond = Long.parseLong(second);
        Date date = new Date(mSecond * 1000);
        DateFormat df = new SimpleDateFormat("d MMM yyyy");//"2h - 14 May 2015"
        return df.format(date);
    }

    public static String formatRating(String rating) {
        String returnString = "";
        try {
            double rate = Double.valueOf(rating);
            return new DecimalFormat("#.##").format(rate);
        } catch (Exception ex) {
            return returnString;
        }

    }

    public static String getLinkMovie(String link) {
        String url = "";
        Pattern pattern = Pattern.compile("http([^#^\\s^\\n\\r^@]*)(\\.mp4|\\.flv|\\.ism|\\.mkv|\\.webm)");
        Matcher matcher = pattern.matcher(link);
        while (matcher.find()) {
            if (StringUtils.isNotEmpty(matcher.group(0))) {
                url = matcher.group(0);
            }
        }
        return url;

    }

    public static String getLinkTvChannel(String link) {
        String url = "";
        Pattern pattern = Pattern.compile("http([^#^\\s^\\n\\r^@]*)(\\.mp4|\\.flv|\\.ism|\\.mkv|\\.webm|\\.m3u8)");
        Matcher matcher = pattern.matcher(link);
        while (matcher.find()) {
            if (StringUtils.isNotEmpty(matcher.group(0))) {
                url = matcher.group(0);
            }
        }
        return url;

    }

    public static boolean checkExpireToken(long expire) {
        if (expire < (System.currentTimeMillis() / 1000)) {
            return false;
        }
        return true;
    }

    public static boolean checkRelogin() {
        if (StringUtils.isNotEmpty(ApolloApplication.getInstance().
                getAppSettings().getApolloToken())) {
            if (Utils.checkExpireToken(ApolloApplication.getInstance().
                    getAppSettings().getTokenExpire())) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public static void checkValidToken(String token, final SimpleCallBack callBack) {
        if (StringUtils.isEmpty(token)) {
            callBack.failed();
        } else {
            UserLoader.getInstance().checkToken(token, new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    callBack.success(true);
                }

                @Override
                public void failed() {
                    callBack.failed();
                }
            });
        }
    }

    /**
     * Test if this device is a Google TV.
     * <p>
     * See 32:00 in "Google I/O 2011: Building Android Apps for Google TV"
     * http://www.youtube.com/watch?v=CxLL-sR6XfM
     *
     * @return true if google tv
     */
    public static boolean isGoogleTV(Context context) {
        UiModeManager uiModeManager = (UiModeManager) context.getSystemService(UI_MODE_SERVICE);
        if (uiModeManager.getCurrentModeType() == Configuration.UI_MODE_TYPE_TELEVISION) {
            return true;
        } else {
            return false;
        }
    }
}
