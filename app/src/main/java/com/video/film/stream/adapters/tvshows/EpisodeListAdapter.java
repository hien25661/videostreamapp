package com.video.film.stream.adapters.tvshows;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.video.film.stream.R;
import com.video.film.stream.adapters.movies.MovieListAdapter;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.models.tvshows.Episode;
import com.video.film.stream.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class EpisodeListAdapter extends UltimateViewAdapter<EpisodeListAdapter.ViewHolder>{
    private ArrayList<Episode> episodes;
    private Context mContext;
    private SelectorListenerObject callback;

    public EpisodeListAdapter(ArrayList<Episode> episodes, SelectorListenerObject callback) {
        this.episodes = episodes;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_tvshows_episode, parent, false);
        ViewHolder vh = new ViewHolder(v);
        mContext = v.getContext();
        return vh;
    }

    @Override
    public ViewHolder newFooterHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder newHeaderHolder(View view) {
        return null;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (position >= 0 && position < episodes.size()) {
            final Episode episode = episodes.get(position);
            if (episode != null) {
                Utils.showImageFromUrlIntoView(mContext,episode.getScreenshot(),holder.imvFilm,position);
                holder.tvTitle.setText("EP "+(position+1)+" "+episode.getTitle());

                holder.tvSeasonInfo.setText(Utils.getDateFromSecondSeason(episode.getReleased()));
                holder.tvLast.setText(episode.getPlot());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(callback!=null) {
                            callback.onSelected(episode);
                        }
                    }
                });
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        return null;
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getAdapterItemCount() {
        return episodes.size();
    }

    @Override
    public long generateHeaderId(int position) {
        return 0;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.imvFilm)
        ImageView imvFilm;
        @Bind(R.id.tvTitle)
        CustomTextView tvTitle;
        @Bind(R.id.tvSeasonInfo)
        CustomTextView tvSeasonInfo;
        @Bind(R.id.tvLast)
        CustomTextView tvLast;
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public void onViewRecycled(ViewHolder holder) {
        try {
            if (holder != null) {
                Glide.clear(holder.imvFilm);
            }
        } catch (Exception ex) {
        }
        super.onViewRecycled(holder);
    }
}
