package com.video.film.stream.models.tvshows;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Objects;

/**
 * Created by nguyenvanhien on 7/2/17.
 */

public class LastSeason implements Parcelable{
    private String season;
    private String episode;
    private String released;
    private String title;
    private String plot;
    private String screenshot;

    protected LastSeason(Parcel in) {
        season = in.readString();
        episode = in.readString();
        released = in.readString();
        title = in.readString();
        plot = in.readString();
        screenshot = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(season);
        dest.writeString(episode);
        dest.writeString(released);
        dest.writeString(title);
        dest.writeString(plot);
        dest.writeString(screenshot);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LastSeason> CREATOR = new Creator<LastSeason>() {
        @Override
        public LastSeason createFromParcel(Parcel in) {
            return new LastSeason(in);
        }

        @Override
        public LastSeason[] newArray(int size) {
            return new LastSeason[size];
        }
    };

    public String getSeason() {
        return season;
    }

    public String getEpisode() {
        return episode;
    }

    public String getReleased() {
        return released;
    }

    public String getTitle() {
        return title;
    }

    public String getPlot() {
        return plot;
    }

    public String getScreenshot() {
        return screenshot;
    }
}
