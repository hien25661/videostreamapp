package com.video.film.stream.fragment.liveTv;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.video.film.stream.R;
import com.video.film.stream.adapters.TvChanel.TvChanelListAdapter;
import com.video.film.stream.adapters.tvshows.TvShowListAdapter;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.loader.MovieLoader;
import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.movies.MovieCategory;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.ViewType;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class LiveTvFragment extends Fragment {
    @Bind(R.id.rcvMovie)
    UltimateRecyclerView rcvMovie;

    private Context mContext;
    private MovieCategory movieCategory;
    private TvChanelListAdapter movieListAdapter;
    private MovieLoader movieLoader;
    private ArrayList<TvChanel> tvShows;

    public LiveTvFragment(){

    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    public static LiveTvFragment newInstance(MovieCategory movieCate) {
        LiveTvFragment fragment = new LiveTvFragment();
        Bundle bundle = new Bundle(1);
        bundle.putParcelable(Consts.CATEGORY, movieCate);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie, container, false);
        ButterKnife.bind(this,view);
        mContext = view.getContext();
        movieLoader = MovieLoader.getInstance();
        initView();
        if(movieListAdapter == null) {
            loadData();
        }else {
            rcvMovie.setAdapter(movieListAdapter);
        }
        return view;
    }

    private void initView() {
        rcvMovie.setHasFixedSize(true);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(mContext, 3);
        if (Utils.isGoogleTV(mContext)) {
            rcvMovie.setLayoutManager(gridLayoutManager);
        } else {
            rcvMovie.setLayoutManager(layoutManager);
        }
    }

    private void loadData() {
        movieCategory = getArguments().getParcelable(Consts.CATEGORY);
        if(movieCategory!=null) {
            movieLoader.getLiveTvList(ViewType.TVCHANEL.getType(), movieCategory.getId(), new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    tvShows = (ArrayList<TvChanel>) params[0];
                    setAdapterMovie(tvShows);
                }

                @Override
                public void failed() {

                }
            });
        }
    }

    private void setAdapterMovie(ArrayList<TvChanel> movies) {
        movieListAdapter = new TvChanelListAdapter(movies);
        rcvMovie.setAdapter(movieListAdapter);
    }

    @Override
    public void onDestroy() {
        recycleFragmentView();
        super.onDestroy();
    }

    private void recycleFragmentView(){
        try{
            Glide.get(mContext).getBitmapPool().clearMemory();
            Glide.get(mContext).clearMemory();
        }catch (Exception ex){

        }
    }
}
