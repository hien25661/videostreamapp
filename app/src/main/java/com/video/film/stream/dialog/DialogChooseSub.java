package com.video.film.stream.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Window;
import android.view.WindowManager;

import com.video.film.stream.R;
import com.video.film.stream.adapters.subtitle.ChooseSubsDialogAdapter;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.models.sub.SubTitle;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 7/10/17.
 */

public class DialogChooseSub extends Dialog {
    @Bind(R.id.rcvPopUp)
    RecyclerView rcvPopUp;
    private Context mContext;
    private ArrayList<SubTitle> subTitles;
    private ChooseSubsDialogAdapter chooseSubsDialogAdapter;
    private SelectorListenerObject callBack;
    public DialogChooseSub(@NonNull Context context, ArrayList<SubTitle> list, SelectorListenerObject callBack) {
        super(context);
        this.mContext = context;
        this.subTitles = list;
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_choose_sub);
        ButterKnife.bind(this);
        getWindow().setLayout(WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        rcvPopUp.setHasFixedSize(false);
        rcvPopUp.setLayoutManager(new LinearLayoutManager(mContext));
        chooseSubsDialogAdapter = new ChooseSubsDialogAdapter(subTitles, new SelectorListenerObject() {
            @Override
            public void onSelected(Object... params) {
                callBack.onSelected(params[0]);
            }
        });
        rcvPopUp.setAdapter(chooseSubsDialogAdapter);
    }
}
