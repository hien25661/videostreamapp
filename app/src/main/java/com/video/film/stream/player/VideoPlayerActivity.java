package com.video.film.stream.player;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.devbrackets.android.exomedia.ui.widget.VideoView;
import com.devbrackets.android.playlistcore.listener.PlaylistListener;
import com.devbrackets.android.playlistcore.manager.BasePlaylistManager;
import com.devbrackets.android.playlistcore.service.PlaylistServiceCore;
import com.video.film.stream.ApolloApplication;
import com.video.film.stream.R;
import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.player.data.MediaItem;
import com.video.film.stream.player.data.Samples;
import com.video.film.stream.player.manager.PlaylistManager;
import com.video.film.stream.player.playlist.VideoApi;

import java.util.LinkedList;
import java.util.List;


public class VideoPlayerActivity extends Activity implements PlaylistListener<MediaItem> {
    public static final String EXTRA_INDEX = "EXTRA_INDEX";
    public static final String MOVIE = "MOVIE";
    public static final String TVSHOWS = "TVSHOWS";
    public static final String TVCHANNEL = "TVCHANNEL";
    public static final int PLAYLIST_ID = 6; //Arbitrary, for the example (different from audio)

    protected VideoView videoView;
    protected PlaylistManager playlistManager;

    protected int selectedIndex = 0;
    protected boolean pausedInOnStop = false;
    protected Movie movie;
    protected TvShow tvShow;
    protected TvChanel tvChanel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_player_activity);

        retrieveExtras();
        init();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (videoView.isPlaying()) {
            pausedInOnStop = true;
            videoView.pause();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (pausedInOnStop) {
            videoView.start();
            pausedInOnStop = false;
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        playlistManager.unRegisterPlaylistListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        playlistManager = ApolloApplication.getPlaylistManager();
        playlistManager.registerPlaylistListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playlistManager.invokeStop();
    }

    @Override
    public boolean onPlaylistItemChanged(MediaItem currentItem, boolean hasNext, boolean hasPrevious) {
        return false;
    }

    @Override
    public boolean onPlaybackStateChanged(@NonNull PlaylistServiceCore.PlaybackState playbackState) {
        if (playbackState == PlaylistServiceCore.PlaybackState.STOPPED) {
            finish();
            return true;
        } else if (playbackState == PlaylistServiceCore.PlaybackState.ERROR) {
            showErrorMessage();
        }

        return false;
    }

    /**
     * Retrieves the extra associated with the selected playlist index
     * so that we can start playing the correct item.
     */
    protected void retrieveExtras() {
        Bundle extras = getIntent().getExtras();
        movie = extras.getParcelable(MOVIE);
        tvShow = extras.getParcelable(TVSHOWS);
        tvChanel = extras.getParcelable(TVCHANNEL);
    }

    protected void init() {
        if (movie != null) {
            setupPlaylistMovieManager();

            videoView = (VideoView) findViewById(R.id.video_play_activity_video_view);

            playlistManager.setVideoPlayer(new VideoApi(videoView));
            playlistManager.play(0, false);
        }else if(tvShow!=null){
            setupPlaylistTvShowManager();

            videoView = (VideoView) findViewById(R.id.video_play_activity_video_view);

            playlistManager.setVideoPlayer(new VideoApi(videoView));
            playlistManager.play(0, false);
        } else if(tvChanel!=null){
            setupPlaylistTvChannelManager();

            videoView = (VideoView) findViewById(R.id.video_play_activity_video_view);

            playlistManager.setVideoPlayer(new VideoApi(videoView));
            playlistManager.play(0, false);
        }
    }

    protected void showErrorMessage() {
        new AlertDialog.Builder(this)
                .setTitle("Playback Error")
                .setMessage(String.format("There was an error playing \"%s\"", playlistManager.getCurrentItem().getTitle()))
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                })
                .show();
    }

    /**
     * Retrieves the playlist instance and performs any generation
     * of content if it hasn't already been performed.
     */
    private void setupPlaylistMovieManager() {
        playlistManager = ApolloApplication.getPlaylistManager();

        List<MediaItem> mediaItems = new LinkedList<>();
        Samples.Sample sample = new Samples.Sample(movie.getTitle(),movie.getLink());
        /*for (Samples.Sample sample : Samples.getVideoSamples()) {
            MediaItem mediaItem = new MediaItem(sample, false);
            mediaItems.add(mediaItem);
        }*/
        MediaItem mediaItem = new MediaItem(sample, false);
        mediaItems.add(mediaItem);

        playlistManager.setAllowedMediaType(BasePlaylistManager.AUDIO | BasePlaylistManager.VIDEO);
        playlistManager.setParameters(mediaItems, selectedIndex);
        playlistManager.setId(PLAYLIST_ID);
    }

    private void setupPlaylistTvShowManager() {
        playlistManager = ApolloApplication.getPlaylistManager();

        List<MediaItem> mediaItems = new LinkedList<>();
        Samples.Sample sample = new Samples.Sample(tvShow.getTitle(),tvShow.getLink());
        /*for (Samples.Sample sample : Samples.getVideoSamples()) {
            MediaItem mediaItem = new MediaItem(sample, false);
            mediaItems.add(mediaItem);
        }*/
        MediaItem mediaItem = new MediaItem(sample, false);
        mediaItems.add(mediaItem);

        playlistManager.setAllowedMediaType(BasePlaylistManager.AUDIO | BasePlaylistManager.VIDEO);
        playlistManager.setParameters(mediaItems, selectedIndex);
        playlistManager.setId(PLAYLIST_ID);
    }

    private void setupPlaylistTvChannelManager() {
        playlistManager = ApolloApplication.getPlaylistManager();

        List<MediaItem> mediaItems = new LinkedList<>();
        Log.e("LINK",""+tvChanel.getLink());
        Samples.Sample sample = new Samples.Sample(tvChanel.getName(),tvChanel.getLink());
        /*for (Samples.Sample sample : Samples.getVideoSamples()) {
            MediaItem mediaItem = new MediaItem(sample, false);
            mediaItems.add(mediaItem);
        }*/
        MediaItem mediaItem = new MediaItem(sample, false);
        mediaItems.add(mediaItem);

        playlistManager.setAllowedMediaType(BasePlaylistManager.AUDIO | BasePlaylistManager.VIDEO);
        playlistManager.setParameters(mediaItems, selectedIndex);
        playlistManager.setId(PLAYLIST_ID);
    }
}
