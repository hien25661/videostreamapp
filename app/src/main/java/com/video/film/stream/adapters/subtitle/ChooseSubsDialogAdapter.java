package com.video.film.stream.adapters.subtitle;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.video.film.stream.R;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.models.sub.SubTitle;
import com.video.film.stream.models.tvshows.Seasons;
import com.video.film.stream.utils.StringUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 7/3/17.
 */

public class ChooseSubsDialogAdapter extends RecyclerView.Adapter<ChooseSubsDialogAdapter.ViewHolder> {
    private ArrayList<SubTitle> seasons;
    private Context mContext;
    private SelectorListenerObject callback;
    private int type;

    public ChooseSubsDialogAdapter(ArrayList<SubTitle> list, SelectorListenerObject callback) {
        this.seasons = list;
        this.callback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_choose_language, parent, false);
        ViewHolder vh = new ViewHolder(v);
        mContext = v.getContext();
        return vh;
    }

    /*
    type = 0 : adapter for dialog choose season
    type = 1 : adapter for all seasons view
     */
    int selectedPosition = 0;
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (position >= 0 && position < seasons.size()) {
            final SubTitle item = seasons.get(position);
            if (item != null) {
                holder.tvTitle.setText(item.getCountryName());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedPosition = position;
                        notifyDataSetChanged();
                        callback.onSelected(item, position);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return seasons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvTitle)
        CustomTextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
