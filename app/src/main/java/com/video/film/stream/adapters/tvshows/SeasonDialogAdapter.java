package com.video.film.stream.adapters.tvshows;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.video.film.stream.R;
import com.video.film.stream.callbacks.CallBack;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.models.tvshows.Seasons;
import com.video.film.stream.utils.StringUtils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by nguyenvanhien on 7/3/17.
 */

public class SeasonDialogAdapter extends RecyclerView.Adapter<SeasonDialogAdapter.ViewHolder> {
    private ArrayList<Seasons> seasons;
    private Context mContext;
    private SelectorListenerObject callback;
    private int type;

    public SeasonDialogAdapter(ArrayList<Seasons> list, int type, SelectorListenerObject callback) {
        this.seasons = list;
        this.callback = callback;
        this.type = type;
    }

    public SeasonDialogAdapter(ArrayList<Seasons> list, int type, SelectorListenerObject callback, int selectedPosition) {
        this.seasons = list;
        this.callback = callback;
        this.type = type;
        this.selectedPosition = selectedPosition;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(type == 0 ? R.layout.item_choose_season : R.layout.item_choose_all_season, parent, false);
        ViewHolder vh = new ViewHolder(v);
        mContext = v.getContext();
        return vh;
    }

    /*
    type = 0 : adapter for dialog choose season
    type = 1 : adapter for all seasons view
     */
    int selectedPosition = 0;
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        if (position >= 0 && position < seasons.size()) {
            final Seasons item = seasons.get(position);
            if (item != null) {
                if (type == 0) {
                    if (StringUtils.isNotEmpty(item.getSeasonName())) {
                        holder.tvTitle.setText("SEASON " + item.getSeasonName());
                    }
                } else if (type == 1) {
                    if (StringUtils.isNotEmpty(item.getSeasonName())) {
                        holder.tvTitle.setText("Season " + item.getSeasonName()
                                + "( "+item.getEpisodes().size()
                                + (item.getEpisodes().size() > 1 ? " episodes )" : " episode )"));
                    }
                }
                if (selectedPosition == position) {
                    holder.tvTitle.setTextColor(Color.parseColor("#2BBAE0"));
                    selectedPosition = -1;
                }
                else {
                    if(type == 0){
                        holder.tvTitle.setTextColor(Color.WHITE);
                    }else {
                        holder.tvTitle.setTextColor(Color.parseColor("#5D5F65"));
                    }
                }
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        selectedPosition = position;
                        notifyDataSetChanged();
                        callback.onSelected(item, position);
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return seasons.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.tvTitle)
        CustomTextView tvTitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
