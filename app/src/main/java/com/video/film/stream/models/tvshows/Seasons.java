package com.video.film.stream.models.tvshows;

import android.os.Parcel;
import android.os.Parcelable;

import com.video.film.stream.models.BaseModel;

import java.util.ArrayList;

/**
 * Created by nguyenvanhien on 7/2/17.
 */

public class Seasons extends BaseModel implements Parcelable {
    private String seasonName;
    ArrayList<Episode> episodes = new ArrayList<>();

    public Seasons(){}
    protected Seasons(Parcel in) {
        seasonName = in.readString();
        episodes = in.createTypedArrayList(Episode.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(seasonName);
        dest.writeTypedList(episodes);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Seasons> CREATOR = new Creator<Seasons>() {
        @Override
        public Seasons createFromParcel(Parcel in) {
            return new Seasons(in);
        }

        @Override
        public Seasons[] newArray(int size) {
            return new Seasons[size];
        }
    };

    public String getSeasonName() {
        return seasonName;
    }

    public ArrayList<Episode> getEpisodes() {
        return episodes;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    public void setEpisodes(ArrayList<Episode> episodes) {
        this.episodes = episodes;
    }
}
