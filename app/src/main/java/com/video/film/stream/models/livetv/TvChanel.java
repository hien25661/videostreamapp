package com.video.film.stream.models.livetv;

import android.os.Parcel;
import android.os.Parcelable;

import com.video.film.stream.models.BaseModel;

/**
 * Created by nguyenvanhien on 7/4/17.
 */

public class TvChanel extends BaseModel implements Parcelable {
    private String id;
    private String name;
    private String hd;
    private String logo;
    private String epg_now;
    private String epg_next;
    private String link;

    public TvChanel() {
    }

    protected TvChanel(Parcel in) {
        id = in.readString();
        name = in.readString();
        hd = in.readString();
        logo = in.readString();
        epg_now = in.readString();
        epg_next = in.readString();
        link = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(name);
        dest.writeString(hd);
        dest.writeString(logo);
        dest.writeString(epg_now);
        dest.writeString(epg_next);
        dest.writeString(link);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TvChanel> CREATOR = new Creator<TvChanel>() {
        @Override
        public TvChanel createFromParcel(Parcel in) {
            return new TvChanel(in);
        }

        @Override
        public TvChanel[] newArray(int size) {
            return new TvChanel[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHd() {
        return hd;
    }

    public void setHd(String hd) {
        this.hd = hd;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getEpg_now() {
        return epg_now;
    }

    public void setEpg_now(String epg_now) {
        this.epg_now = epg_now;
    }

    public String getEpg_next() {
        return epg_next;
    }

    public void setEpg_next(String epg_next) {
        this.epg_next = epg_next;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
