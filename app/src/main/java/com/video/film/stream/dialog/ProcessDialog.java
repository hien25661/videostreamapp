package com.video.film.stream.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.video.film.stream.R;
import com.video.film.stream.utils.StringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by hien.nv on 5/10/17.
 */

public class ProcessDialog extends Dialog {
    private String message;
    @Bind(R.id.tvMessage)
    TextView tvMessage;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ProcessDialog(@NonNull Context context) {
        super(context);
    }

    public ProcessDialog(@NonNull Context context, String message) {
        super(context);
        this.message = message;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        setContentView(R.layout.layout_progessbar);
        ButterKnife.bind(this);
        setCanceledOnTouchOutside(false);
        setCancelable(false);

        if (StringUtils.isNotEmpty(message)) {
            tvMessage.setText(message);
            tvMessage.setVisibility(View.VISIBLE);
        } else {
            tvMessage.setVisibility(View.GONE);
        }
    }
}
