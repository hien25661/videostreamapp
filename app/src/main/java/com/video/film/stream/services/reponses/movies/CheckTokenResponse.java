package com.video.film.stream.services.reponses.movies;

import com.video.film.stream.models.BaseModel;

/**
 * Created by nguyenvanhien on 7/5/17.
 */

public class CheckTokenResponse extends BaseModel {
    private String token;
    private String result;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
