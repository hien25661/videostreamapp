package com.video.film.stream.dialog;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.video.film.stream.ApolloApplication;
import com.video.film.stream.R;
import com.video.film.stream.activity.FullscreenActivity;
import com.video.film.stream.activity.LoginDialog;
import com.video.film.stream.activity.TvShowDetailActivity;
import com.video.film.stream.activity.VideoViewSubtitleNew;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.livetv.TvChanel;
import com.video.film.stream.models.tvshows.Seasons;
import com.video.film.stream.models.tvshows.TvShow;
import com.video.film.stream.player.FullScreenVideoPlayerActivity;
import com.video.film.stream.player.VideoPlayerActivity;
import com.video.film.stream.utils.AppSettings;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.StringUtils;
import com.video.film.stream.utils.Utils;
import com.video.film.stream.utils.WatchListAction;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyenvanhien on 6/28/17.
 */

public class DialogInfoLiveTv extends Dialog {
    @Bind(R.id.imvPoster)
    ImageView imvPoster;
    @Bind(R.id.tvName)
    TextView tvName;
    @Bind(R.id.tvInfo)
    TextView tvInfo;

    @Bind(R.id.tvInfo1)
    TextView tvInfo1;
    @Bind(R.id.tvRating)
    CustomTextView tvRating;
    @Bind(R.id.tvNameLastEpisode)
    CustomTextView tvNameLastEpisode;
    @Bind(R.id.imvWatchList)
    ImageView imvWatchList;

    @Bind(R.id.frame_playTrailer)
    FrameLayout frame_playTrailer;

    private TvChanel tvShow;
    private Context mContext;
    private UserLoader userLoader;
    private LoginDialog loginDialog;
    private ProcessDialog processDialog;
    private AppSettings appSettings;

    private boolean isActionAdd = false;
    private boolean isFromWatchList = false;

    private SelectorListenerObject deleteCallback;

    public DialogInfoLiveTv(@NonNull Context context) {
        super(context, R.style.full_screen_dialog);
        this.mContext = context;
    }

    public void setTvChanel(TvChanel movie) {
        this.tvShow = movie;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_info_livetv);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);
        userLoader = UserLoader.getInstance();
        processDialog = new ProcessDialog(mContext);
        appSettings = ApolloApplication.getInstance().getAppSettings();
        loginDialog = new LoginDialog(mContext, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                if (isActionAdd) {
                    addOrRemoveWatchList();
                    isActionAdd = false;
                } else {
                    playMovie();
                }
            }

            @Override
            public void failed() {

            }
        });
        setData();
    }

    private void setData() {
        if (tvShow != null) {
            Utils.showImageFromUrlIntoView(mContext, tvShow.getLogo(), imvPoster, 0);
            tvName.setText(tvShow.getName());
            if (appSettings.isChannelBelongWatchList(tvShow.getId())) {
                imvWatchList.setImageResource(R.mipmap.ic_remove_watchlist);
            } else {
                imvWatchList.setImageResource(R.mipmap.ic_top_dialog);
            }

            if(isFromWatchList){
                frame_playTrailer.setVisibility(View.GONE);
            }
        }
    }

    @OnClick(R.id.btnClose)
    public void closeDialog() {
        this.dismiss();
    }

    @OnClick(R.id.btnWatch)
    public void watch() {
        Utils.checkValidToken(ApolloApplication.getInstance().getAppSettings().getApolloToken(),
                new SimpleCallBack() {
                    @Override
                    public void success(Object... params) {
                        playMovie();
                    }

                    @Override
                    public void failed() {
                        loginDialog.show();
                    }
                });

    }

    private void playMovie(){
        try {
            processDialog.show();

            userLoader.playTvChannel(tvShow
                    , new SimpleCallBack() {
                        @Override
                        public void success(Object... params) {
                            processDialog.dismiss();
                            if (params[0] != null && params[0] instanceof String) {
                                tvShow.setLink((String) params[0]);
                                Intent intent = new Intent(mContext, VideoViewSubtitleNew.class);
                                intent.putExtra("url", tvShow.getLink());
                                intent.putExtra("name", tvShow.getName());
                                intent.putExtra("liveTV",true);
                                mContext.startActivity(intent);
                            }
                        }

                        @Override
                        public void failed() {
                            processDialog.dismiss();
                        }
                    });
        }catch (Exception ex){
            processDialog.dismiss();
        }
    }


    public void addOrRemoveWatchList() {
        if (!appSettings.isChannelBelongWatchList(tvShow.getId())) {
            userLoader.addWatchListChannel(tvShow.getId(), new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    imvWatchList.setImageResource(R.mipmap.ic_remove_watchlist);
                    if(deleteCallback!=null){
                        deleteCallback.onSelected(WatchListAction.ADD);
                    }
                }

                @Override
                public void failed() {

                }
            });

        } else {
            userLoader.removeWatchListChannel(tvShow.getId(), new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    imvWatchList.setImageResource(R.mipmap.ic_top_dialog);
                    if(deleteCallback!=null) {
                        deleteCallback.onSelected(WatchListAction.DELETE);
                    }
                }

                @Override
                public void failed() {

                }
            });

        }
    }
    @OnClick(R.id.imvWatchList)
    public void addToWatchList() {
        isActionAdd = true;
        Utils.checkValidToken(ApolloApplication.getInstance().getAppSettings().getApolloToken()
                , new SimpleCallBack() {
                    @Override
                    public void success(Object... params) {
                        addOrRemoveWatchList();
                    }

                    @Override
                    public void failed() {
                        loginDialog.show();
                    }
                });

    }
    public void setDeleteCallback(SelectorListenerObject deleteCallback) {
        this.deleteCallback = deleteCallback;
    }

    public void setFromWatchList(boolean fromWatchList) {
        isFromWatchList = fromWatchList;
    }
}
