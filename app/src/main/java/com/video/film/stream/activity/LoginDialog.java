package com.video.film.stream.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.video.film.stream.ApolloApplication;
import com.video.film.stream.BaseActivity;
import com.video.film.stream.R;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.dialog.ProcessDialog;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.utils.StringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyenvanhien on 6/30/17.
 */

public class LoginDialog extends Dialog{
    @Bind(R.id.edtUserName)
    EditText edtUserName;
    @Bind(R.id.edtPassWord)
    EditText edtPassWord;
    @Bind(R.id.btnLogin)
    TextView btnLogin;
    private UserLoader userLoader;
    private ProcessDialog progressDialog;
    private Context mContext;
    private SimpleCallBack callBack;

    public LoginDialog(@NonNull Context context, SimpleCallBack callBack) {
        super(context, R.style.full_screen_dialog);
        this.mContext = context;
        this.callBack = callBack;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.MATCH_PARENT);
        ButterKnife.bind(this);
        initView();
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    private void initView() {
        userLoader = UserLoader.getInstance();
        progressDialog = new ProcessDialog(mContext);
        edtPassWord.setTypeface(ApolloApplication.getInstance().getCustomFont());
        edtUserName.setTypeface(ApolloApplication.getInstance().getCustomFont());
    }
    @OnClick(R.id.btnLogin)
    public void loginApollo(){
        String userName = edtUserName.getText().toString();
        String password = edtPassWord.getText().toString();
        if(StringUtils.isNotEmpty(userName) && StringUtils.isNotEmpty(password)){
            progressDialog.show();
            userLoader.login(userName.trim(), password.trim(), new SimpleCallBack() {
                @Override
                public void success(Object... params) {
                    progressDialog.dismiss();
                    LoginDialog.this.dismiss();
                    callBack.success();
                }

                @Override
                public void failed() {
                    progressDialog.dismiss();
                    Toast.makeText(mContext,"Incorrect username or password",Toast.LENGTH_LONG).show();
                }
            });
        }
    }
    private void startMainActivity(){
        Intent intent = new Intent(mContext, MainActivity.class);
        mContext.startActivity(intent);
        dismiss();
    }
}
