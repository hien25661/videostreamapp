package com.video.film.stream.utils;

/**
 * Created by nguyenvanhien on 6/30/17.
 */

public class Consts {
    public static final String CATEGORY = "CATEGORY";
    public static final String MOVIE = "MOVIE";
    public static final String EPISODE = "EPISODE";
    public static final String WATCHLIST_MOVIE = "WATCHLIST_MOVIE";
    public static final String WATCHLIST_TVSHOWS= "WATCHLIST_TVSHOWS";
    public static final String WATCHLIST_TVCHANELS= "WATCHLIST_TVCHANELS";

    public static final String SEARCH = "SEARCH";
}
