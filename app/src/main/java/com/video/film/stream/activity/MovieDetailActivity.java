package com.video.film.stream.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.video.film.stream.ApolloApplication;
import com.video.film.stream.BaseActivity;
import com.video.film.stream.R;
import com.video.film.stream.callbacks.SelectorListenerObject;
import com.video.film.stream.callbacks.SimpleCallBack;
import com.video.film.stream.customview.CustomTextView;
import com.video.film.stream.dialog.DialogChooseSub;
import com.video.film.stream.dialog.ProcessDialog;
import com.video.film.stream.loader.MovieLoader;
import com.video.film.stream.loader.UserLoader;
import com.video.film.stream.models.movies.Movie;
import com.video.film.stream.models.sub.SubTitle;
import com.video.film.stream.player.FullScreenVideoPlayerActivity;
import com.video.film.stream.player.VideoPlayerActivity;
import com.video.film.stream.utils.Consts;
import com.video.film.stream.utils.StringUtils;
import com.video.film.stream.utils.Utils;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by nguyenvanhien on 7/1/17.
 */

public class MovieDetailActivity extends BaseActivity {
    @Bind(R.id.imvPoster)
    ImageView imvPoster;
    @Bind(R.id.tvTitle)
    CustomTextView tvTitle;
    @Bind(R.id.tvInfo)
    CustomTextView tvInfo;
    @Bind(R.id.tvNumLikes)
    CustomTextView tvNumLikes;
    @Bind(R.id.tvNumComments)
    CustomTextView tvNumComments;
    @Bind(R.id.tvRating)
    CustomTextView tvRating;
    @Bind(R.id.tvSynopis)
    CustomTextView tvSynopis;
    @Bind(R.id.tvYear)
    CustomTextView tvYear;

    private Movie movie;
    private UserLoader userLoader;
    private ProcessDialog processDialog;
    private LoginDialog loginDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
        userLoader = UserLoader.getInstance();
        processDialog = new ProcessDialog(this);
        loginDialog = new LoginDialog(this, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                playMovie();
            }

            @Override
            public void failed() {

            }
        });
        initData();
    }

    private void initData() {
        movie = getIntent().getParcelableExtra(Consts.MOVIE);
        if (movie != null) {
            loadInfoDetail(movie);
        }
    }

    private void loadInfoDetail(Movie movie) {
        Utils.showImageFromUrlIntoView(this,movie.getPoster(),imvPoster,0);
        tvTitle.setText(movie.getTitle());
        tvInfo.setText(movie.getGenres()+" - "+movie.getTimeRelease());
        tvNumComments.setCommentText(movie.getRuntime());
        tvNumLikes.setLikeText(movie.getVotes());
        tvRating.setRatingBig(movie.getRating());
        tvSynopis.setText(movie.getPlot());
        tvYear.setYearText("MOVIE",movie.getYear());
    }
    @OnClick(R.id.imvBack)
    public void onBack(){
        onBackPressed();
    }

    @OnClick(R.id.imvPlay)
    public void onPlay(){
       Utils.checkValidToken(ApolloApplication.
                       getInstance().getAppSettings().getApolloToken(),
               new SimpleCallBack() {
                   @Override
                   public void success(Object... params) {
                       playMovie();
                   }

                   @Override
                   public void failed() {
                       loginDialog.show();
                   }
               });
    }
    private DialogChooseSub dialogChooseSub;
    private void playMovie(){
        processDialog.show();
        MovieLoader movieLoader = MovieLoader.getInstance();
        movieLoader.searchSubTitle(movie.getImdb(), new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                ArrayList<SubTitle> subList = new ArrayList<SubTitle>();
                subList = (ArrayList<SubTitle>) params[0];
                processDialog.dismiss();
                if(subList !=null && subList.size() >0) {
                    SubTitle header = new SubTitle();
                    header.setHeader(true);
                    header.setCountryName("None");
                    subList.add(0,header);
                    dialogChooseSub = new DialogChooseSub(
                            MovieDetailActivity.this, subList, new SelectorListenerObject() {
                        @Override
                        public void onSelected(Object... params) {
                            if (params[0] != null && params[0] instanceof SubTitle) {
                                startMoviePlayer((SubTitle) params[0]);
                            }
                        }
                    }
                    );
                    dialogChooseSub.show();
                }else {
                    startMoviePlayer(null);
                }

            }

            @Override
            public void failed() {
                startMoviePlayer(null);
            }
        });
    }

    private void startMoviePlayer(final SubTitle sub) {
        userLoader.playMovie(movie, new SimpleCallBack() {
            @Override
            public void success(Object... params) {
                processDialog.dismiss();
                if (params[0] != null && params[0] instanceof String) {
                    movie.setLink((String) params[0]);
                    Intent intent = new Intent(MovieDetailActivity.this, VideoViewSubtitleNew.class);
                    intent.putExtra("url", movie.getLink());
                    intent.putExtra("name", movie.getTitle());
                    if (sub != null) {
                        if (StringUtils.isNotEmpty(sub.getSubUrl())) {
                            intent.putExtra("sub", sub.getSubUrl());
                        }
                    }
                    startActivity(intent);
                    if(dialogChooseSub!=null && dialogChooseSub.isShowing()){
                        dialogChooseSub.dismiss();
                    }
                }
            }

            @Override
            public void failed() {
                processDialog.dismiss();
            }
        });
    }
    @Override
    protected void onDestroy() {
        try{
            Glide.clear(imvPoster);
        }catch (Exception e){}
        super.onDestroy();
    }
}
