package com.video.film.stream.services.reponses.movies;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.video.film.stream.models.BaseModel;
import com.video.film.stream.models.movies.MovieCategory;

import java.util.ArrayList;

/**
 * Created by nguyenvanhien on 6/27/17.
 */

public class MovieCategoryResponse extends BaseModel {
    @SerializedName("result")
    @Expose
    private ArrayList<MovieCategory> listCategory = new ArrayList<>();

    public ArrayList<MovieCategory> getMovieCategory() {
        return listCategory;
    }

}
