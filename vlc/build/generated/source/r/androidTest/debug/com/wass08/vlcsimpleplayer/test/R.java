/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package com.wass08.vlcsimpleplayer.test;

public final class R {
    public static final class attr {
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int measureBasedOnAspectRatio=0x7f010006;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int metaButtonBarButtonStyle=0x7f010001;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int metaButtonBarStyle=0x7f010000;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int useDefaultControls=0x7f010002;
        /** <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int useTextureViewBacking=0x7f010003;
        /** <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>center</code></td><td>0</td><td></td></tr>
<tr><td><code>centerCrop</code></td><td>1</td><td></td></tr>
<tr><td><code>centerInside</code></td><td>2</td><td></td></tr>
<tr><td><code>fitCenter</code></td><td>3</td><td></td></tr>
<tr><td><code>none</code></td><td>4</td><td></td></tr>
</table>
         */
        public static final int videoScale=0x7f010007;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int videoViewApiImpl=0x7f010004;
        /** <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
         */
        public static final int videoViewApiImplLegacy=0x7f010005;
    }
    public static final class color {
        public static final int black_overlay=0x7f050000;
        public static final int exomedia_default_controls_button_disabled=0x7f050001;
        public static final int exomedia_default_controls_button_normal=0x7f050002;
        public static final int exomedia_default_controls_button_pressed=0x7f050003;
        public static final int exomedia_leanback_progress_horizontal_background=0x7f050004;
        public static final int exomedia_leanback_progress_horizontal_progress_placeholder=0x7f050005;
        public static final int exomedia_leanback_progress_horizontal_progress_secondary=0x7f050006;
    }
    public static final class drawable {
        public static final int ic_action_pause_over_video=0x7f020000;
        public static final int ic_action_play_over_video=0x7f020001;
        public static final int ic_launcher=0x7f020002;
    }
    public static final class id {
        public static final int action_settings=0x7f080014;
        public static final int button_start=0x7f080013;
        public static final int center=0x7f080000;
        public static final int centerCrop=0x7f080001;
        public static final int centerInside=0x7f080002;
        public static final int fitCenter=0x7f080003;
        public static final int layoutControl=0x7f080008;
        public static final int layoutTitle=0x7f08000d;
        public static final int none=0x7f080004;
        public static final int textView=0x7f080011;
        public static final int url_getter=0x7f080012;
        public static final int vlc_button_play_pause=0x7f080009;
        public static final int vlc_container=0x7f080005;
        public static final int vlc_duration=0x7f08000b;
        public static final int vlc_duration1=0x7f08000c;
        public static final int vlc_overlay=0x7f080007;
        public static final int vlc_overlay_des=0x7f080010;
        public static final int vlc_overlay_subtitle=0x7f08000f;
        public static final int vlc_overlay_title=0x7f08000e;
        public static final int vlc_seekbar=0x7f08000a;
        public static final int vlc_surface=0x7f080006;
    }
    public static final class layout {
        public static final int activity_fullscreen_vlc_player=0x7f030000;
        public static final int activity_media_selector=0x7f030001;
    }
    public static final class menu {
        public static final int menu_media_selector=0x7f070000;
    }
    public static final class string {
        public static final int action_settings=0x7f060000;
        public static final int app_name=0x7f060001;
        public static final int button_start=0x7f060002;
        public static final int default_url=0x7f060003;
        public static final int dummy_button=0x7f060004;
        public static final int dummy_content=0x7f060005;
        public static final int instruction=0x7f060006;
        public static final int title_activity_fullscreen_vlc_player=0x7f060007;
        public static final int url1=0x7f060008;
    }
    public static final class style {
        /**  Customize your theme here. 
         */
        public static final int AppTheme=0x7f040002;
        public static final int ButtonBar=0x7f040003;
        public static final int ButtonBarButton=0x7f040004;
        public static final int FullscreenActionBarStyle=0x7f040000;
        public static final int FullscreenTheme=0x7f040001;
    }
    public static final class styleable {
        /** Attributes that can be used with a ButtonBarContainerTheme.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_metaButtonBarButtonStyle com.wass08.vlcsimpleplayer.test:metaButtonBarButtonStyle}</code></td><td></td></tr>
           <tr><td><code>{@link #ButtonBarContainerTheme_metaButtonBarStyle com.wass08.vlcsimpleplayer.test:metaButtonBarStyle}</code></td><td></td></tr>
           </table>
           @see #ButtonBarContainerTheme_metaButtonBarButtonStyle
           @see #ButtonBarContainerTheme_metaButtonBarStyle
         */
        public static final int[] ButtonBarContainerTheme = {
            0x7f010000, 0x7f010001
        };
        /**
          <p>This symbol is the offset where the {@link com.wass08.vlcsimpleplayer.test.R.attr#metaButtonBarButtonStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.wass08.vlcsimpleplayer.test:metaButtonBarButtonStyle
        */
        public static final int ButtonBarContainerTheme_metaButtonBarButtonStyle = 1;
        /**
          <p>This symbol is the offset where the {@link com.wass08.vlcsimpleplayer.test.R.attr#metaButtonBarStyle}
          attribute's value can be found in the {@link #ButtonBarContainerTheme} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.wass08.vlcsimpleplayer.test:metaButtonBarStyle
        */
        public static final int ButtonBarContainerTheme_metaButtonBarStyle = 0;
        /** Attributes that can be used with a VideoView.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #VideoView_measureBasedOnAspectRatio com.wass08.vlcsimpleplayer.test:measureBasedOnAspectRatio}</code></td><td></td></tr>
           <tr><td><code>{@link #VideoView_useDefaultControls com.wass08.vlcsimpleplayer.test:useDefaultControls}</code></td><td></td></tr>
           <tr><td><code>{@link #VideoView_useTextureViewBacking com.wass08.vlcsimpleplayer.test:useTextureViewBacking}</code></td><td></td></tr>
           <tr><td><code>{@link #VideoView_videoScale com.wass08.vlcsimpleplayer.test:videoScale}</code></td><td></td></tr>
           <tr><td><code>{@link #VideoView_videoViewApiImpl com.wass08.vlcsimpleplayer.test:videoViewApiImpl}</code></td><td></td></tr>
           <tr><td><code>{@link #VideoView_videoViewApiImplLegacy com.wass08.vlcsimpleplayer.test:videoViewApiImplLegacy}</code></td><td></td></tr>
           </table>
           @see #VideoView_measureBasedOnAspectRatio
           @see #VideoView_useDefaultControls
           @see #VideoView_useTextureViewBacking
           @see #VideoView_videoScale
           @see #VideoView_videoViewApiImpl
           @see #VideoView_videoViewApiImplLegacy
         */
        public static final int[] VideoView = {
            0x7f010002, 0x7f010003, 0x7f010004, 0x7f010005,
            0x7f010006, 0x7f010007
        };
        /**
          <p>This symbol is the offset where the {@link com.wass08.vlcsimpleplayer.test.R.attr#measureBasedOnAspectRatio}
          attribute's value can be found in the {@link #VideoView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.wass08.vlcsimpleplayer.test:measureBasedOnAspectRatio
        */
        public static final int VideoView_measureBasedOnAspectRatio = 4;
        /**
          <p>This symbol is the offset where the {@link com.wass08.vlcsimpleplayer.test.R.attr#useDefaultControls}
          attribute's value can be found in the {@link #VideoView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.wass08.vlcsimpleplayer.test:useDefaultControls
        */
        public static final int VideoView_useDefaultControls = 0;
        /**
          <p>This symbol is the offset where the {@link com.wass08.vlcsimpleplayer.test.R.attr#useTextureViewBacking}
          attribute's value can be found in the {@link #VideoView} array.


          <p>Must be a boolean value, either "<code>true</code>" or "<code>false</code>".
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name com.wass08.vlcsimpleplayer.test:useTextureViewBacking
        */
        public static final int VideoView_useTextureViewBacking = 1;
        /**
          <p>This symbol is the offset where the {@link com.wass08.vlcsimpleplayer.test.R.attr#videoScale}
          attribute's value can be found in the {@link #VideoView} array.


          <p>Must be one of the following constant values.</p>
<table>
<colgroup align="left" />
<colgroup align="left" />
<colgroup align="left" />
<tr><th>Constant</th><th>Value</th><th>Description</th></tr>
<tr><td><code>center</code></td><td>0</td><td></td></tr>
<tr><td><code>centerCrop</code></td><td>1</td><td></td></tr>
<tr><td><code>centerInside</code></td><td>2</td><td></td></tr>
<tr><td><code>fitCenter</code></td><td>3</td><td></td></tr>
<tr><td><code>none</code></td><td>4</td><td></td></tr>
</table>
          @attr name com.wass08.vlcsimpleplayer.test:videoScale
        */
        public static final int VideoView_videoScale = 5;
        /**
          <p>This symbol is the offset where the {@link com.wass08.vlcsimpleplayer.test.R.attr#videoViewApiImpl}
          attribute's value can be found in the {@link #VideoView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.wass08.vlcsimpleplayer.test:videoViewApiImpl
        */
        public static final int VideoView_videoViewApiImpl = 2;
        /**
          <p>This symbol is the offset where the {@link com.wass08.vlcsimpleplayer.test.R.attr#videoViewApiImplLegacy}
          attribute's value can be found in the {@link #VideoView} array.


          <p>Must be a reference to another resource, in the form "<code>@[+][<i>package</i>:]<i>type</i>:<i>name</i></code>"
or to a theme attribute in the form "<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>".
          @attr name com.wass08.vlcsimpleplayer.test:videoViewApiImplLegacy
        */
        public static final int VideoView_videoViewApiImplLegacy = 3;
    };
}
